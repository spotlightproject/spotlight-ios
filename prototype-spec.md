Spotlight for iOS 
iOS App Prototype Specification v0.1
Febrary 02, 2020
Nathan Freitas

App MUST work on recent version of iOS version 13.x and WatchOS 6.x

App MUST provide some integration with a paired Apple Watch to support the receiption of richer data

App MUST provide an onboarding flow that matches the screens and questions in the Android app. 
* Most recent onboarding design is at: https://invis.io/6WVGGCPM35A
- App MUST allow user to set start and stop work time
- App MUST allow user to indicate which days of week they work
- App MUST allow user to set home and work locations

App MUST provide a "Study Picker" user interface on the phone app similar to the Android app, to enable, configure and view capture of specific types of data
* Most recent study flow is at: https://invis.io/V7VJU94GEZM#/400723108_Home

App MUST enable a simple user interface on the Watch to allow user to indicate start/stop commute, and add a simple "Work Log" entry

App MUST track daily movement, ideally using Step count data captured on an at least an HOURLY basis, retrieved from HeatlhKit

App MUST enable the entry into a Work Log matching the form UI of the Android app 

App MUST provide a way for user to set their HOME and WORK locations, ideally through an interactive map

App MUST track and store location data of the user, or their watch, on the highest frequency possible

App MUST display a users daily distance traveled

App SHOULD display daily location data on a map

App MUST provide a way to export captured data in CSV format for a specific time range, matching the format of the Android app

Sample data:
12377,"screen","Jan 10, 2020 2:41:17 PM","on"
12376,"steps","Jan 10, 2020 2:41:16 PM","182.0"
12375,"motion","Jan 10, 2020 2:41:16 PM","5.6386347"
12370,"geo_logging","Jan 10, 2020 1:12:57 PM","42.3286016,-71.1250371,50.007,0.0,-10.699999809265137,0.0"

App MAY provide access and log Environmental Noise readings from HealthKit
App MAY log screen on/off, charging state, and other sensors as available on iOS

App MAY render any data captured using third-party chart/visualization components


