import SwiftUI

struct CountdownCircle: Shape {
	public var degrees: Double
	public var size: CGSize
	
	var animatableData: Double {
	   get { return degrees }
	   set { degrees = newValue }
	}

	func path(in rect: CGRect) -> Path {
		var path = Path()

		path.addArc(
			center: CGPoint(x: size.width / 2, y: size.height / 2),
			radius: ((size.height / 2) - 10),
			startAngle: .degrees(0),
			endAngle: .degrees(degrees),
			clockwise: false
		)

		return path
	}
}
