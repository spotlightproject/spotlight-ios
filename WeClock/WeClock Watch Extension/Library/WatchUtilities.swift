import Foundation

fileprivate var ConsolePrefix = ">>"

func debug(_ input: Any?, data: Any? = nil) {
	#if DEBUG
		var output: String
		var additionalOutput: String? = nil
	
		if let data = data {
			additionalOutput = String(describing: data)
		}
		
		if let unwrapped = input {
			let string = String(describing: unwrapped)
			output = "\(string)"
		} else {
			output = "nil"
		}
	
		//Print debug event
		print("\(ConsolePrefix) \(output)")
		if let additionalOutput = additionalOutput {
			print("\(ConsolePrefix) { \(additionalOutput) }")
		}
	#endif
}
