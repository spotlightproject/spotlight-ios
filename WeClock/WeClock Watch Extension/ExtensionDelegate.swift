import WatchKit
import WatchConnectivity
import HealthKit
import CoreLocation
import UserNotifications

class ExtensionDelegate: NSObject, WKExtensionDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate {
	
	//MARK: WCSession
	public var wcSession: WCSession?
	public var commuteManager = CommuteManager()
	let locationServices = LocationServices.shared
	
	//MARK: Extension Delegate
    func applicationDidFinishLaunching() {
        beginConnectivitySession()
		authorizeHealthKit()
		authorizeNotifications()
		locationServices.authorize()
    }
	
	//MARK: Notifications
	func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		completionHandler([.sound, .alert])
	}
	
	private func authorizeNotifications() {
		UNUserNotificationCenter.current().delegate = self
		UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert]) { (success, error) in
			if let error = error { print("Error authorizing watch notifications: \(error)") }
			if success { print("Watch notifications authorized.") }
		}
	}
	
    func applicationDidBecomeActive() {}

    func applicationWillResignActive() {}
	
	//MARK: WCSession Setup
	func beginConnectivitySession() {
		if WCSession.isSupported() {
			wcSession = WCSession.default
			wcSession?.delegate = self
			wcSession?.activate()
		} else {
			print("WCSession not supported")
		}
	}

	//MARK: Background Tasks
    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            case let relevantShortcutTask as WKRelevantShortcutRefreshBackgroundTask:
                // Be sure to complete the relevant-shortcut task once you're done.
                relevantShortcutTask.setTaskCompletedWithSnapshot(false)
            case let intentDidRunTask as WKIntentDidRunRefreshBackgroundTask:
                // Be sure to complete the intent-did-run task once you're done.
                intentDidRunTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }
}

//MARK: HealthKit Authorization
extension ExtensionDelegate {

	public func authorizeHealthKit() {
		let routeType = HKSeriesType.workoutRoute()
		let workoutType = HKWorkoutType.workoutType()
		
		var readTypes = HKQuantityType.quantityTypes(for: [
			.stepCount,
			.appleStandTime,
			.appleExerciseTime,
			.distanceWalkingRunning,
			.heartRate,
			.basalEnergyBurned,
			.activeEnergyBurned
		])
		readTypes.insert(workoutType)
		readTypes.insert(routeType)
		
		var writeTypes = HKQuantityType.quantityTypes(for: [
			.stepCount,
			.distanceWalkingRunning,
			.heartRate,
			.flightsClimbed,
			.activeEnergyBurned,
			.basalEnergyBurned
		])
		writeTypes.insert(workoutType)
		writeTypes.insert(routeType)
		
		let am = HKAuthManager(readTypes: readTypes, writeTypes: writeTypes)
		am.authorize {
			debug("HealthKit authorized!")
		}
	}
}

//MARK: WCSession Delegate
extension ExtensionDelegate: WCSessionDelegate {
	
	//MARK: WCSession Delegate
	func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
		print("Activation complete")
	}
}
