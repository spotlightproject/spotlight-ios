import SwiftUI

struct TodayView: View {
	
	@ObservedObject public var viewModel = MovementViewModel(timePeriod: .today)
	
	var body: some View {
		List {
			QuantityCellView(
				title: "Steps",
				unit: String(),
				amount: viewModel.steps,
				roundingGuide: .noDecimals)
			QuantityCellView(
				title: "Distance",
				unit: "miles",
				amount: viewModel.walkingDistance,
				roundingGuide: .onePlace)
			QuantityCellView(
				title: "Stand Time",
				unit: "minutes",
				amount: viewModel.standMinutes,
				roundingGuide: .noDecimals)
			QuantityCellView(
				title: "Exercise Time",
				unit: "minutes",
				amount: viewModel.exerciseMinutes,
				roundingGuide: .noDecimals)
		}.navigationBarTitle("Movement")
	}
}

struct Today_Preview: PreviewProvider {
	static var previews: some View {
		TodayView()
	}
}
