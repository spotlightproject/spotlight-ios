import SwiftUI

struct WorkLogView: View {
	
	//MARK: Environment
	@Environment(\.presentationMode) var presentationMode
	
	//MARK: Data Sources
	@State private var selection: Int = 0
	
	//MARK: Properties
	var logTypes: [String] {
		WorkLogType.allCases.map { $0.rawValue }
	}
	
	var body: some View {
		VStack {
			Picker("Log Type", selection: $selection) {
				ForEach(0..<self.logTypes.count) { index in
					Text(self.logTypes[index]).tag(index)
				}
			}
			Button(action: {
				let loggedType = self.logTypes[self.selection]
				print("Tapped on: \(loggedType)")
				self.askForWorkLogTransfer(logType: loggedType)
				self.presentationMode.wrappedValue.dismiss()
			}) {
				Text("Save")
			}
			.foregroundColor(.black)
			.background(Color.yellow)
			.cornerRadius(10)
		}
	}
	
	//MARK: Methods
	func askForWorkLogTransfer(logType: String) {
		let delegate = (WKExtension.shared().delegate as! ExtensionDelegate)
		let dict: [String: Any] = ["eventType": logType, "dateRecorded": Date()]
		delegate.wcSession!.transferUserInfo(dict)
	}
}

struct WorkLog_Preview: PreviewProvider {
	static var previews: some View {
		WorkLogView()
	}
}
