import SwiftUI
import Combine

struct CommuteView: View {
	
	//MARK: Environment
	@EnvironmentObject var commuteManager: CommuteManager
	
	//MARK: Data Sources
	@ObservedObject var circleStates = TimedStates(initialState: 360 as Double, subsequentStates: [240, 120, 0])
	@ObservedObject var countStates = TimedStates(initialState: 3, subsequentStates: [2, 1, 0])
	
	var body: some View {
		VStack {
			if countStates.didFinish {
				CommuteInProgressView()
			} else {
				CountdownView(
					circleStates: circleStates,
					countStates: countStates
				)
			}
		}
		.onAppear {
			self.commuteManager.startWorkout()
		}
	}
}
