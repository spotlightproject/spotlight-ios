import SwiftUI

struct HomeView: View {
	
	//MARK: Environment
	@EnvironmentObject var commuteManager: CommuteManager
	
	//MARK: Data Sources
	@State private var isPresentingCommute: Bool = false
	@State private var isPresentingWorkLog: Bool = false
	
	var body: some View {
		VStack(alignment: .center, spacing: 10) {
			Button(action: {
				self.isPresentingCommute.toggle()
			}) {
				Text("Start Commute")
			}
			.sheet(isPresented: $isPresentingCommute, onDismiss: {
				self.commuteManager.stopWorkout()
			}, content: {
				CommuteView().environmentObject(self.commuteManager)
			})
			.foregroundColor(.black)
			.background(Color("brand-primary"))
			.cornerRadius(10)
			Button(action: {
				self.isPresentingWorkLog.toggle()
			}) {
				Text("Add Work Log")
			}
			.sheet(isPresented: $isPresentingWorkLog, content: {
				WorkLogView()
			})
			.foregroundColor(.black)
			.background(Color("brand-secondary"))
			.cornerRadius(10)
		}
		.navigationBarTitle("WeClock")
	}
}

struct HomeView_Preview: PreviewProvider {
	static var previews: some View {
		HomeView()
	}
}
