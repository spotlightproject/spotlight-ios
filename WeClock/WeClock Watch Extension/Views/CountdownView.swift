import SwiftUI

struct CountdownView: View {
	
	//MARK: Data Sources
	@ObservedObject public var circleStates: TimedStates<Double>
	@ObservedObject public var countStates: TimedStates<Int>
	
	//MARK: Properties
	let strokeStyle = StrokeStyle(lineWidth: 12)
	
	let gradient = LinearGradient(gradient: Gradient(colors: [.green, .blue]), startPoint: .topLeading, endPoint: .bottomTrailing)
	
	let backgroundGradient: LinearGradient = {
		let gradient = Gradient(colors: [
			Color("ring-background-one"),
			Color("ring-background-two")
		])
		return LinearGradient(gradient: gradient, startPoint: .topLeading, endPoint: .bottomTrailing)
	}()
	
	var body: some View {
		GeometryReader { geometry in
			ZStack {
				Text("\(self.countStates.currentState)")
					.foregroundColor(.green)
					.font(Font.system(size: 50))
					.bold()
				CountdownCircle(degrees: 360, size: geometry.size).stroke(self.backgroundGradient, style: self.strokeStyle)
				CountdownCircle(degrees: self.circleStates.currentState, size: geometry.size)
					.stroke(self.gradient, style: self.strokeStyle)
					.animation(.default)
			}
		}
	}
}

struct CountdownView_Previews: PreviewProvider {
	
	//MARK: Properties
	static var doubleStates: TimedStates<Double> = TimedStates(initialState: 0, subsequentStates: [0])
	
	static var intStates: TimedStates<Int> = TimedStates(initialState: 0, subsequentStates: [0])
	
	static var previews: some View {
		CountdownView(circleStates: doubleStates, countStates: intStates)
	}
}
