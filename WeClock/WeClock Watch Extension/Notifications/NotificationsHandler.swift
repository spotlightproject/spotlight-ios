import Foundation
import UserNotifications

class NotificationsManager {
	
	//MARK: Shared Instance
	public static var shared = NotificationsManager()
	
	func scheduleNotification() {
		print("Scheduling notification...")
		
		let mutable = UNMutableNotificationContent()
		mutable.title = "Reminder"
		mutable.body = "Did you forget to stop your commute?"
		mutable.sound = .default
		
		let request = UNNotificationRequest(identifier: UUID().uuidString, content: mutable, trigger: nil)
		
		UNUserNotificationCenter.current().add(request) { (error) in
			if let error = error { print("Error adding notification request \(error)") }
		}
	}
}
