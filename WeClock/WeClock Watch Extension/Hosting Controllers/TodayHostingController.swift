import WatchKit
import Foundation
import SwiftUI

class TodayHostingController: WKHostingController<TodayView> {
	
	override var body: TodayView {
		TodayView()
	}
}
