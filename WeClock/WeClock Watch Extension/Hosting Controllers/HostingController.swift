import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<AnyView> {
	
	private var commute: CommuteManager {
		let delegate = WKExtension.shared().delegate as! ExtensionDelegate
		return delegate.commuteManager
	}
	
	override var body: AnyView {
		AnyView(
			HomeView().environmentObject(commute)
		)
	}
}
