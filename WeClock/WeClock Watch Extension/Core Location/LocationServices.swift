import Foundation
import CoreLocation

class LocationServices {
	
	public static let shared = LocationServices()
	public let locationManager = CLLocationManager()
	
	public func authorize() {
		locationManager.requestAlwaysAuthorization()
	}
}
