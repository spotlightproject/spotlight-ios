import Combine
import Foundation
import HealthKit
import CoreLocation

class CommuteManager: NSObject, ObservableObject, CLLocationManagerDelegate {
	
	//MARK: Properties
	private let healthStore = HKHealthStore()
	private var previousReminderTime: TimeInterval = 0
	
	lazy var configuration: HKWorkoutConfiguration = {
		let configuration = HKWorkoutConfiguration()
		configuration.activityType = .running
		configuration.locationType = .unknown
		return configuration
	}()
	
	
	public var session: HKWorkoutSession!
	public var builder: HKLiveWorkoutBuilder!
	public var dataSource: HKLiveWorkoutDataSource!
	public var routeBuilder: HKWorkoutRouteBuilder!
	
	//MARK: Workout Management
	func startWorkout() {
		previousReminderTime = 0
		configureDataSource()
		configureSession()
		configureBuilder()
		configureRouteBuilder()
		
		session.startActivity(with: Date())
		builder.beginCollection(withStart: Date()) { (success, error) in
			guard success else {
				debug("Collection failure: \(error as Any)")
				return
			}
			debug("Session started successfully :)")
		}
		
		//Route building
		LocationServices.shared.locationManager.delegate = self
		LocationServices.shared.locationManager.startUpdatingLocation()
	}
	
	public func stopWorkout() {
		print("Stopping workout...")
		LocationServices.shared.locationManager.stopUpdatingLocation()
//		session.stopActivity(with: Date()); session.end()
		session.end()
		builder.endCollection(withEnd: Date()) { (success, error) in
			if let error = error { debug("Error", data: error) }
			if success { debug("Builder ended collection.") }
			
			self.builder.finishWorkout { (workout, error) in
				if let error = error { debug("Error finishing workout", data: error) }
				guard let workout = workout else { return }
				
				self.routeBuilder.finishRoute(with: workout, metadata: nil) { (route, error) in
					if let _ = error { debug("Error finishing route") }
					if let _ = route { debug("Saved route") }
				}
			}
		}
	}
	
	//MARK: Configuration
	private func configureRouteBuilder() {
		routeBuilder = HKWorkoutRouteBuilder(healthStore: healthStore, device: HKDevice.local())
	}
	
	private func configureSession() {
		do {
			session = try HKWorkoutSession(healthStore: healthStore, configuration: configuration)
			builder = session.associatedWorkoutBuilder()
			debug("Workout intialized successfully.")
		} catch {
			debug("Workout initialization failed.", data: error)
			return
		}
		session.delegate = self
	}
	
	private func configureBuilder() {
		builder.dataSource = dataSource
		builder.delegate = self
		builder.shouldCollectWorkoutEvents = true
		builder.addMetadata([Keys.CustomWorkoutType: Identifiers.CommuteWorkout]) { (success, error) in
			if let error = error { debug("Error adding metadata", data: error) }
			if success { debug("Successfully saved metadata") }
		}
	}
	
	private func configureDataSource() {
		dataSource = HKLiveWorkoutDataSource(healthStore: healthStore, workoutConfiguration: configuration)
		let distance = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
		let activeEnergy = HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!
		let basalEnergy = HKQuantityType.quantityType(forIdentifier: .basalEnergyBurned)!
		let flights = HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!
		dataSource.enableCollection(for: activeEnergy, predicate: nil)
		dataSource.enableCollection(for: flights, predicate: nil)
		dataSource.enableCollection(for: basalEnergy, predicate: nil)
		dataSource.enableCollection(for: distance, predicate: nil)
	}
}

//MARK: WorkoutBuilder Delegate
extension CommuteManager: HKLiveWorkoutBuilderDelegate {
	func workoutBuilder(_ workoutBuilder: HKLiveWorkoutBuilder, didCollectDataOf collectedTypes: Set<HKSampleType>) {
		debug("sample types: \(collectedTypes)")
		sendReminderIfNeccesary(elapsedTime: workoutBuilder.elapsedTime)
	}
	
	func workoutBuilderDidCollectEvent(_ workoutBuilder: HKLiveWorkoutBuilder) {
		debug("builder received event:")
		if let lastEvent = workoutBuilder.workoutEvents.last {
			debug("last event: \(lastEvent)")
		} else {
			debug("no event")
		}
		debug("Elapsed time: \(builder?.elapsedTime as Any)")
	}
	
	private func sendReminderIfNeccesary(elapsedTime: TimeInterval) {
		let timeOfFirstReminder: TimeInterval = (30 * 60)
		let intervalBetweenReminders: TimeInterval = (5 * 60)
		
		if elapsedTime > timeOfFirstReminder
		&& elapsedTime > previousReminderTime + intervalBetweenReminders {
			NotificationsManager.shared.scheduleNotification()
			previousReminderTime = elapsedTime
		}
	}
}

//MARK: Workout Session Delegate
extension CommuteManager: HKWorkoutSessionDelegate {
	
	func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
		debug("session state changed: \(toState.rawValue)")
	}
	
	func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
		debug("Session failed with error: \(error)")
	}
	
	func workoutSession(_ workoutSession: HKWorkoutSession, didGenerate event: HKWorkoutEvent) {
		debug("session generated event: \(event)")
	}
	
}

// MARK: CLLocationManager Delegate
extension CommuteManager {
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let filteredLocations = locations.filter { (location: CLLocation) -> Bool in
			location.horizontalAccuracy <= 50.0
		}
		
		guard !filteredLocations.isEmpty else { return }
		
		routeBuilder.insertRouteData(filteredLocations) { (success, error) in
			if !success {
				debug("Failed to insert route.", data: error)
			} else {
				debug("Successfully inserted route!")
			}
		}
	}
}
