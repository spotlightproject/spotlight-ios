import Foundation
import CoreData
import Combine

class LocationsStore: NSObject, ObservableObject, NSFetchedResultsControllerDelegate, SummaryProvider, StudyStore, Searchable, Deletable {
	
	//MARK: Observables
	@Published public var locations = [StoredLocation]() {
		didSet {
			summaryDescription = "\(locations.crudeDistance.string(roundedTo: .onePlace)) miles"
		}
	}
	var convertibles = CurrentValueSubject<[StudyResultConvertible], Never>([])
	
	//MARK: Summary Provider
	@Published public var summaryDescription: String = String()
	public var descriptionPublisher: Published<String>.Publisher { $summaryDescription }
	
	//MARK: Initialization
	init(timePeriod: HKModel.TimePeriod? = nil) {
		super.init()
		if let timePeriod = timePeriod {
			filter(by: timePeriod)
		} else {
			configureFRC(predicate: nil)
		}
	}
	
	//MARK: Fetching
	public var frc: NSFetchedResultsController<StoredLocation>!
	
	public func configureFRC(predicate: NSPredicate?) {
		let fetch = StoredLocation.fetchRequest() as NSFetchRequest<StoredLocation>
		fetch.predicate = predicate
		fetch.sortDescriptors = [NSSortDescriptor(keyPath: \StoredLocation.dateVisited, ascending: false)]
		
		let controller = NSFetchedResultsController<StoredLocation>(fetchRequest: fetch, managedObjectContext: CoreData.shared.viewContext, sectionNameKeyPath: nil, cacheName: nil)
		controller.delegate = self
		try? controller.performFetch()
		
		self.frc = controller
		publishFetch()
	}
	
	public func reconfigureFRC(predicate: NSPredicate?) {
		configureFRC(predicate: predicate)
		publishFetch()
	}
	
	//MARK: Updating
	public func update() {
		try? frc.performFetch()
		publishFetch()
	}
	
	public func reset() {
		reconfigureFRC(predicate: nil)
	}
	
	//MARK: Deletion
	func delete() {
		for entry in frc.fetchedObjects ?? [] {
			CoreData.shared.viewContext.delete(entry)
		}
		CoreData.shared.saveContext()
	}
	
	//MARK: Filtering
	var filtering = CurrentValueSubject<Bool, Never>(false)
	@Published public var storedFilterDate: Date?
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	public func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		updateFilterDate(for: timePeriod)
		let filterPredicate = predicate(for: timePeriod)
		reconfigureFRC(predicate: filterPredicate)
	}
	
	private func updateFilterDate(for timePeriod: HKModel.TimePeriod) {
		switch timePeriod {
			case .specificDay(day: let date): storedFilterDate = date
			case .today: storedFilterDate = Date()
			default: storedFilterDate = nil
		}
	}
	
	func unfilter() {
		filtering.send(false)
		reconfigureFRC(predicate: nil)
	}
	
	private func predicate(for timePeriod: HKModel.TimePeriod) -> NSPredicate {
		switch timePeriod {
			case .specificDay(day: let target):
				let calendar = Calendar.current
				let todayComponents = calendar.dateComponents([.day, .month, .year], from: target)
				let startDate = calendar.date(from: todayComponents)!
				let endDate = calendar.date(byAdding: DateComponents(day: 1), to: startDate)!
				let startPredicate = NSPredicate(format: "dateVisited >= %@", startDate as NSDate)
				let endPredicate = NSPredicate(format: "dateVisited <= %@", endDate as NSDate)
				let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [startPredicate, endPredicate])
				return predicate
			default:
				let startDate = Date.startDate(of: timePeriod)!
				let predicate = NSPredicate(format: "dateVisited >= %@", startDate as NSDate)
				return predicate
		}
	}
	
	//MARK: Search
	var isSearching = CurrentValueSubject<Bool, Never>(false)
	
	public func search(for query: String) {
		var subPredicates = [NSPredicate]()
        subPredicates.append(NSPredicate(format: "placeName CONTAINS[c] %@", query))
		subPredicates.append(NSPredicate(format: "streetName CONTAINS[c] %@", query))
		subPredicates.append(NSPredicate(format: "cityName CONTAINS[c] %@", query))
		subPredicates.append(NSPredicate(format: "stateAbbreviation CONTAINS[c] %@", query))
		subPredicates.append(NSPredicate(format: "countryName CONTAINS[c] %@", query))
        let compoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: subPredicates)
		reconfigureFRC(predicate: compoundPredicate)
	}
	
	//MARK: Publishing
	private func publishFetch() {
		locations = self.frc.fetchedObjects ?? []
		convertibles.send(frc.fetchedObjects ?? [])
	}
	
	//MARK: FetchedResultsController Delegate
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		publishFetch()
	}
}
