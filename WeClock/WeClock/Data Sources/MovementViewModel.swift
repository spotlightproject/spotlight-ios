import HealthKit
import Combine

class MovementViewModel: ObservableObject, SummaryProvider, Filterable {
	
	//MARK: Observables
	@Published public var steps: Double = 0
	@Published public var exerciseMinutes: Double = 0
	@Published public var standMinutes: Double = 0
	@Published public var walkingDistance: Double = 0
	
	//MARK: Summary Provider
	@Published public var summaryDescription: String = "0 steps"
	public var descriptionPublisher: Published<String>.Publisher { $summaryDescription }
	
	//MARK: Initialization
	internal init(timePeriod: HKModel.TimePeriod) {
		filter(by: timePeriod)
	}
	
	//MARK: HealthKit Interface
	private func fetch(timePeriod: HKModel.TimePeriod) {
		
		let stepsQuery = HKModel.QueryBuilder.collectionQuery(
			identifier: .stepCount,
			timePeriod: timePeriod,
			options: .cumulativeSum) { stats in
				DispatchQueue.main.async {
					let numberOfSteps = self.steps(from: stats) ?? 0
					self.steps = numberOfSteps
					self.summaryDescription = "\(numberOfSteps.string(roundedTo: .noDecimals)) steps"
				}
		}
		
		let standQuery = HKModel.QueryBuilder.collectionQuery(
			identifier: .appleStandTime,
			timePeriod: timePeriod,
			options: .cumulativeSum) { stats in
				DispatchQueue.main.async {
					self.standMinutes = self.minutes(from: stats) ?? 0
				}
		}
		
		let exerciseQuery = HKModel.QueryBuilder.collectionQuery(
			identifier: .appleExerciseTime,
			timePeriod: timePeriod,
			options: .cumulativeSum) { stats in
				DispatchQueue.main.async {
					self.exerciseMinutes = self.minutes(from: stats) ?? 0
				}
		}

		let distanceQuery = HKModel.QueryBuilder.collectionQuery(
			identifier: .distanceWalkingRunning,
			timePeriod: timePeriod,
			options: .cumulativeSum) { (stats) in
				DispatchQueue.main.async {
					self.walkingDistance = self.meters(from: stats) ?? 0
				}
		}
		
		HKModel.shared.execute([
			exerciseQuery,
			standQuery,
			stepsQuery,
			distanceQuery
		])
	}
	
	//MARK: HKUnit Value Extraction
	private func minutes(from statistics: HKStatistics) -> Double? {
		return statistics.sumQuantity()?.doubleValue(for: .minute())
	}
	
	private func steps(from statistics: HKStatistics) -> Double? {
		return statistics.sumQuantity()?.doubleValue(for: .count())
	}
	
	private func meters(from statistics: HKStatistics) -> Double? {
		return statistics.sumQuantity()?.doubleValue(for: .mile())
	}
	
	//MARK: Filterable
	public var filtering = CurrentValueSubject<Bool, Never>(false)
	@Published public var storedFilterDate: Date?
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	public func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		switch timePeriod {
			case .specificDay(day: let date): self.storedFilterDate = date
			case .today: self.storedFilterDate = Date()
			default: self.storedFilterDate = nil
		}
		fetch(timePeriod: timePeriod)
	}
	
	func unfilter() {
		filtering.send(false)
		fetch(timePeriod: .any)
	}
	
}
