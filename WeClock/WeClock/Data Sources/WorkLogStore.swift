import Foundation
import CoreData
import Combine

class WorkLogStore: NSObject, ObservableObject, NSFetchedResultsControllerDelegate, SummaryProvider, StudyStore, Searchable, Deletable {
	
	//MARK: Observables
	public var convertibles = CurrentValueSubject<[StudyResultConvertible], Never>([]) 
	@Published var objects: [WorkLogEntry] = [] {
		didSet { summaryDescription = "\(objects.count) entries" }
	}
	
	//MARK: Summary View Model
	@Published var summaryDescription = String()
	public var descriptionPublisher: Published<String>.Publisher { $summaryDescription }
	
	//MARK: Initialization
	init(timePeriod: HKModel.TimePeriod = .any) {
		super.init()
		let startDate = Date.startDate(of: timePeriod)! as NSDate
		let predicate = NSPredicate(format: "dateRecorded >= %@", startDate)
		configureFRC(predicate: predicate)
	}
	
	//MARK: Fetched Results Controller
	public var frc: NSFetchedResultsController<WorkLogEntry>!
	
	//MARK: Fetching
	public func configureFRC(predicate: NSPredicate?) {
		let fetch = WorkLogEntry.fetchRequest() as NSFetchRequest<WorkLogEntry>
		fetch.predicate = predicate
		fetch.sortDescriptors = [NSSortDescriptor(keyPath: \WorkLogEntry.dateRecorded, ascending: false)]
		
		let controller = NSFetchedResultsController<WorkLogEntry>(fetchRequest: fetch, managedObjectContext: CoreData.shared.viewContext, sectionNameKeyPath: nil, cacheName: nil)
		controller.delegate = self
		try? controller.performFetch()
		
		self.frc = controller
		publishFetch()
	}
	
	public func reconfigureFRC(predicate: NSPredicate?) {
		configureFRC(predicate: predicate)
		publishFetch()
	}
	
	//MARK: Updating
	public func update() {
		try? frc.performFetch()
		publishFetch()
	}
	
	public func reset() {
		reconfigureFRC(predicate: nil)
	}
	
	//MARK: Deletion
	func delete() {
		for entry in frc.fetchedObjects ?? [] {
			CoreData.shared.viewContext.delete(entry)
		}
		CoreData.shared.saveContext()
	}
	
	//MARK: Filtering
	var filtering = CurrentValueSubject<Bool, Never>(false)
	@Published public var storedFilterDate: Date?
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	public func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		let startDate = Date.startDate(of: timePeriod)! as NSDate
		let predicate = NSPredicate(format: "dateRecorded >= %@", startDate)
		reconfigureFRC(predicate: predicate)
	}
	
	func unfilter() {
		filtering.send(false)
		reconfigureFRC(predicate: nil)
	}
	
	//MARK: Search
	var isSearching = CurrentValueSubject<Bool, Never>(false)
	
	public func search(for query: String) {
		var subPredicates = [NSPredicate]()
        subPredicates.append(NSPredicate(format: "notes CONTAINS[c] %@", query))
        subPredicates.append(NSPredicate(format: "eventType CONTAINS[c] %@", query))
        let compoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: subPredicates)
		reconfigureFRC(predicate: compoundPredicate)
	}
	
	//MARK: Publishing
	private func publishFetch() {
		objects = frc.fetchedObjects ?? []
		convertibles.send(frc.fetchedObjects ?? [])
	}
	
	//MARK: FetchedResultsController Delegate
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		publishFetch()
	}
}
