import Foundation
import Combine

class StudyPickerViewModel: ObservableObject {
	
	//MARK: Observables
	@Published var profiles: [StudyProfile] = []
	@Published var isEditing: Bool = false {
		didSet {
			if self.isEditing { self.useAllProfiles() }
			else { self.useOptedInProfiles() }
		}
	}
	
	public func toggleEditingMode() {
		if isEditing {
			isEditing = false
		} else {
			isEditing = true
		}
	}
	
	//MARK: Initialization
	init() {
		useOptedInProfiles()
	}
	
	public func useAllProfiles() {
		self.profiles = StudyProfiles.available
	}
	
	public func useOptedInProfiles() {
		self.profiles = StudyProfiles.available.optedIn
	}
	
	public func update() {
		StudyProfiles.available.forEach { $0.summaryStore.filter(by: .today) }
	}
}
