import Foundation
import CoreData
import Combine

class BatteryStore: NSObject, StudyStore, NSFetchedResultsControllerDelegate, SummaryProvider {
	
	//MARK: Observables
	var convertibles = CurrentValueSubject<[StudyResultConvertible], Never>([])
	
	//MARK: Summary Provider
	@Published private var summaryDescription: String = "0%"
	public var descriptionPublisher: Published<String>.Publisher { $summaryDescription }
	
	//MARK: Initializer
	init(timePeriod: HKModel.TimePeriod? = nil) {
		super.init()
		if let timePeriod = timePeriod {
			filter(by: timePeriod)
		} else {
			configureFRC(predicate: nil)
		}
	}
	
	private func fetch() {
		configureFRC(predicate: nil)
	}
	
	//MARK: Fetched Results Controller
	public var frc: NSFetchedResultsController<BatteryReading>!
	
	//MARK: Fetching
	public func configureFRC(predicate: NSPredicate?) {
		let fetch = BatteryReading.fetchRequest() as NSFetchRequest<BatteryReading>
		fetch.predicate = predicate
		fetch.sortDescriptors = [NSSortDescriptor(keyPath: \BatteryReading.date, ascending: false)]
		
		let controller = NSFetchedResultsController<BatteryReading>(fetchRequest: fetch, managedObjectContext: CoreData.shared.viewContext, sectionNameKeyPath: nil, cacheName: nil)
		controller.delegate = self
		try? controller.performFetch()
		
		self.frc = controller
		publishFetch()
	}
	
	public func reconfigureFRC(predicate: NSPredicate?) {
		configureFRC(predicate: predicate)
		publishFetch()
	}
	
	//MARK: FetchedResultsController Delegate
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		publishFetch()
	}
	
	private func publishFetch() {
		convertibles.send(frc.fetchedObjects ?? [])
		summaryDescription = frc.fetchedObjects?.usage.percentString ?? "%0"
	}
	
	//MARK: Filterable
	public func filter(by timePeriod: HKModel.TimePeriod) {
		debug("filtering")
		filtering.send(true)
		let filterPredicate = predicate(for: timePeriod)
		reconfigureFRC(predicate: filterPredicate)
		updateFilterDate(for: timePeriod)
	}
	
	func unfilter() {
		filtering.send(false)
		reconfigureFRC(predicate: nil)
	}
	
	var filtering = CurrentValueSubject<Bool, Never>(false)
	@Published var storedFilterDate: Date?
	var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	private func updateFilterDate(for timePeriod: HKModel.TimePeriod) {
		switch timePeriod {
			case .specificDay(day: let date): storedFilterDate = date
			case .today: storedFilterDate = Date()
			default: storedFilterDate = nil
		}
	}
	
	private func predicate(for timePeriod: HKModel.TimePeriod) -> NSPredicate {
		switch timePeriod {
			case .specificDay(day: let target):
				let calendar = Calendar.current
				let todayComponents = calendar.dateComponents([.day, .month, .year], from: target)
				let startDate = calendar.date(from: todayComponents)!
				let endDate = calendar.date(byAdding: DateComponents(day: 1), to: startDate)!
				let startPredicate = NSPredicate(format: "date >= %@", startDate as NSDate)
				let endPredicate = NSPredicate(format: "date <= %@", endDate as NSDate)
				let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [startPredicate, endPredicate])
				return predicate
			default:
				let startDate = Date.startDate(of: timePeriod)!
				let predicate = NSPredicate(format: "date >= %@", startDate as NSDate)
				return predicate
		}
	}
	
}

