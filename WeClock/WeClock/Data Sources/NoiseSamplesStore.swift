import Foundation
import HealthKit
import Combine

class NoiseSamplesStore: ObservableObject, StudyStore {
	
	//MARK: Observables
	public var convertibles = CurrentValueSubject<[StudyResultConvertible], Never>([])
	@Published public var storedFilterDate: Date?
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	//MARK: Properties
	private var cancellables = [AnyCancellable]()
	
	//MARK: Initialization
	internal init(timePeriod: HKModel.TimePeriod? = nil) {
		if let timePeriod = timePeriod {
			fetch(timePeriod: timePeriod)
		}
	}
	
	private func fetch(timePeriod: HKModel.TimePeriod) {
		/*
		HKModel.QueryBuilder.anchoredQuery(
			identifier: .environmentalAudioExposure,
			timePeriod: timePeriod,
			completion: { samples in
				debug("found samples")
				self.objects = samples
				self.convertibles.send(samples)
			}, deletionHandler: { _ in })?
		.execute()*/
		
		let noiseFuture = HKModel.QueryBuilder.future(identifier: .environmentalAudioExposure, timePeriod: timePeriod)!
		
		noiseFuture
			.sink { self.convertibles.send($0) }
		.store(in: &cancellables)
		
	}
	
	//MARK: Filterable
	public var filtering = CurrentValueSubject<Bool, Never>(false)
	
	func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		fetch(timePeriod: timePeriod)
	}
	
	func unfilter() {
		filtering.send(false)
		fetch(timePeriod: .any)
	}
	
	func update() {
		///do nothing
	}
	
	func reset() {
		///do nothing
	}
	
	func delete() {
		///can't delete samples we don't own
	}
}
