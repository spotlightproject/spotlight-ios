import Foundation
import Combine

class AllConvertiblesStore: ObservableObject, StudyStore, Deletable {
	
	//MARK: Observables
	public var convertibles = CurrentValueSubject<[StudyResultConvertible], Never>([])
	@Published public var isDeletable: Bool = false
	
	//MARK: Properties
	private var cancellables = [AnyCancellable]()
	
	//MARK: Convertible Sources
	private var moveSamples = MoveSamplesStore(timePeriod: .thisMonth)
	private var locationSamples = LocationsStore(timePeriod: .thisMonth)
	private var workLogs = WorkLogStore(timePeriod: .thisMonth)
	private var commutes = CommuteStore(timePeriod: .thisMonth)
	private var noiseSamples = NoiseSamplesStore(timePeriod: .thisMonth)
	private var batteryReadings = BatteryStore(timePeriod: .thisMonth)
	
	//MARK: Initialization
	internal init(timePeriod: HKModel.TimePeriod? = nil) {
		useSpotlightData()
	}
	
	private func bindSpotlightData() {
		cancellables.forEach { $0.cancel() }
		locationSamples.convertibles
			.combineLatest(workLogs.convertibles, commutes.convertibles, batteryReadings.convertibles)
			.debounce(for: 0.5, scheduler: DispatchQueue(label: "delay"))
			.map { $0.0 + $0.1 + $0.2 + $0.3 }
			.sink { self.convertibles.send($0) }
		.store(in: &cancellables)
	}
	
	private func bindHealthData() {
		cancellables.forEach { $0.cancel() }
		moveSamples.convertibles
			.combineLatest(noiseSamples.convertibles)
			.debounce(for: 0.5, scheduler: DispatchQueue(label: "delay"))
			.map { $0.0 + $0.1 }
			.sink { self.convertibles.send($0) }
		.store(in: &cancellables)
	}
	
	public func useSpotlightData() {
		bindSpotlightData()
		isDeletable = true
	}
	
	public func useHealthData() {
		bindHealthData()
		isDeletable = false
	}
	
	//MARK: Filterable Conformance
	public var filtering = CurrentValueSubject<Bool, Never>(false)
	@Published public var storedFilterDate: Date?
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		moveSamples.filter(by: timePeriod)
		locationSamples.filter(by: timePeriod)
		workLogs.filter(by: timePeriod)
		commutes.filter(by: timePeriod)
		noiseSamples.filter(by: timePeriod)
	}
	
	func unfilter() {
		filtering.send(false)
		moveSamples.unfilter()
		locationSamples.unfilter()
		workLogs.unfilter()
		commutes.unfilter()
		noiseSamples.unfilter()
	}
	
	//MARK: Deletable
	public func delete() {
		locationSamples.delete()
		workLogs.delete()
		commutes.delete()
	}
}
