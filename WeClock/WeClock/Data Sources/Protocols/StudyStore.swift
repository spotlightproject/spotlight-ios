import Foundation
import Combine

/// A store which publishes `StudyResultConvertible` objects.
public protocol StudyConvertiblesStore {
	var convertibles: CurrentValueSubject<[StudyResultConvertible], Never> { get }
}

/// A view model which can represent a study data table. Most store objects will conform to this protocol so they can be displayed by a `StudyDataVC` view controller.
typealias StudyStore = StudyConvertiblesStore & Filterable

public protocol Searchable {
	func search(for query: String)
	func reset()
	var isSearching: CurrentValueSubject<Bool, Never> { get }
}

public protocol Deletable {
	func delete()
}
