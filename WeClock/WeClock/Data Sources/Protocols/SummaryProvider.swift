import Foundation
import Combine

protocol SummaryProvider: Filterable {
	var descriptionPublisher: Published<String>.Publisher { get }
}
