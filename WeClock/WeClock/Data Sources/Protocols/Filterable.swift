import Foundation
import Combine

public protocol Filterable {
	func filter(by timePeriod: HKModel.TimePeriod)
	func unfilter()
	var filtering: CurrentValueSubject<Bool, Never> { get }
	var storedFilterDate: Date? { get set }
	var publishedFilterDate: Published<Date?>.Publisher { get }
}
