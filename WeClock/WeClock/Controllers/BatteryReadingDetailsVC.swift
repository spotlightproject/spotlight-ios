import UIKit

class BatteryReadingDetailsVC: UIViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var levelLabel: BoldLabel!
	@IBOutlet weak var timeLabel: BoldLabel!
	@IBOutlet weak var dateLabel: BoldLabel!
	@IBOutlet weak var sourceLabel: BoldLabel!
	
	//MARK: IBActions
	@IBAction func deleteButtonTapped(_ sender: Any) {
		CoreData.shared.viewContext.delete(reading)
		try! CoreData.shared.viewContext.save()
		navigationController?.popViewController(animated: true)
	}
	
	//MARK: Properties
	public var reading: BatteryReading!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		configure()
	}
	
	public func configure() {
		levelLabel.text = Double(reading.level).percentString
		
		guard let date = reading.date else {
			timeLabel.text = "No Time Recorded"
			dateLabel.text = "No Date Recorded"
			return
		}
		
		/// Date
		timeLabel.text = timeFormatter.string(from: date)
		dateLabel.text = dateFormatter.string(from: date)
		sourceLabel.text = reading.source?.capitalized
	}
	
	private var dateFormatter: DateFormatter {
		let f = DateFormatter()
		f.dateStyle = .short
		f.timeStyle = .none
		return f
	}
	
	private var timeFormatter: DateFormatter {
		let f = DateFormatter()
		f.dateStyle = .none
		f.timeStyle = .short
		return f
	}
}
