import UIKit
import MapKit
import Combine
import JTAppleCalendar

class LocationsTodayVC: UIViewController, MKMapViewDelegate {
	
	//MARK: IBOutlets
	@IBOutlet weak var distanceTodayLabel: UILabel!
	@IBOutlet weak var travelTypeButton: UIButton!
	@IBOutlet weak var mapContainerView: UIView!
	@IBOutlet weak var dateLabel: UILabel!
	
	//MARK: IBActions
	@IBAction func travelButtonTapped(_ sender: Any) {
		showTransportOptions()
	}
	@IBAction func leftButtonTapped(_ sender: Any) {
		viewModel.filter(by: .specificDay(day: viewModel.storedFilterDate!.yesterday!))
	}
	
	@IBAction func rightButtonTapped(_ sender: Any) {
		viewModel.filter(by: .specificDay(day: viewModel.storedFilterDate!.tomorrow!))
	}
	
	//MARK: View Model
	public var viewModel: Filterable!
	private lazy var locationStore = {
		viewModel as! LocationsStore
	}()
	private var cancellables = [AnyCancellable]()
	
	//MARK: Properties
	private var routeManager = RouteManager()
	private let mapComponent = MapComponent()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		routeManager.delegate = self
		
		///Configure map component
		mapComponent.mapView.delegate = self
		mapComponent.snap(to: mapContainerView)
		mapComponent.showLoading()

		///Bind view model
		viewModel.filter(by: HKModel.TimePeriod.specificDay(day: Date()))
		bind()
	}
	
	//MARK: Bind
	private func bind() {
		guard let viewModel = viewModel as? LocationsStore else {
			debug("Couldn't typecase view model.")
			return
		}
		
		viewModel.$locations
			.receive(on: DispatchQueue.main)
			.sink { self.routeManager.start(with: $0) }
		.store(in: &cancellables)
		
		viewModel.publishedFilterDate
			.receive(on: DispatchQueue.main)
			.map { $0?.titleDescription ?? "No Date" }
			.sink { self.dateLabel.text = $0}
		.store(in: &cancellables)
	}
	
	//MARK: Transport Type Handling
	func startCooldown() {
		travelTypeButton.isEnabled = false
		DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
			self.travelTypeButton.isEnabled = true
		}
	}

}

//MARK: Route Drawing
extension LocationsTodayVC: RouteDrawingDelegate {
	
	func draw(routes: [MKRoute], coordinates: [CLLocationCoordinate2D], endCoordinates: [CLLocationCoordinate2D]) {
		mapComponent.mapView.removeOverlays(mapComponent.mapView.overlays)
		mapComponent.mapView.removeAnnotations(mapComponent.mapView.annotations)
		
		debug("drawing routes")
		for route in routes {
			debug("route...")
			self.mapComponent.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
		}
		debug("done drawing routes.")
		
		var annotations = [MKAnnotation]()
		for (index, coordinate) in coordinates.enumerated() {
			let annotation = MKPointAnnotation()
			annotation.title = "\(index)"
			annotation.coordinate = coordinate
			self.mapComponent.mapView.addAnnotation(annotation)
			annotations.append(annotation)
		}
		
		if let lastCoordinate = endCoordinates.last {
			let annotation = MKPointAnnotation()
			annotation.coordinate = lastCoordinate
			annotations.append(annotation)
		}
		
		if !annotations.isEmpty {
			self.mapComponent.mapView.showAnnotations(annotations, animated: true )
		}
		
		//Done drawing. Update UI here
		distanceTodayLabel.text = routeManager.accumulatedMiles.string(roundedTo: .onePlace)
		mapComponent.showMap()
	}
	
	//MARK: MKMapViewDelegate
	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		let renderer = MKPolylineRenderer(overlay: overlay)
		renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)
		renderer.lineWidth = 4.0

		return renderer
	}
}

//MARK: Alert Controller
extension LocationsTodayVC {
	
	func showTransportOptions() {
		let auto = UIAlertAction(title: "Automobile", style: .default) { (action) in
			self.update(to: "Automobile", transportType: .automobile)
		}
		
		let walking = UIAlertAction(title: "Walking", style: .default) { (action) in
			self.update(to: "Walking", transportType: .walking)
		}
		
		let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

		let ac = UIAlertController(title: "Transport Type", message: "Choose a transportation type for drawing routes.", preferredStyle: .actionSheet)
		
		ac.addAction(auto)
		ac.addAction(walking)
		ac.addAction(cancel)
		
		present(ac, animated: true, completion: nil)
	}
	
	func update(to title: String, transportType: MKDirectionsTransportType) {
		routeManager.transportType = transportType
		UIView.animate(withDuration: 0.15) {
			self.travelTypeButton.setTitle(title, for: .normal)
		}
		locationStore.update()
		startCooldown()
	}
}
