import UIKit
import Combine
import JTAppleCalendar

class StudyCalendarVC: UIViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var calendarLabel: UILabel!
	@IBOutlet weak var calendarContainer: UIView!
	@IBOutlet weak var calendarView: JTACMonthView!
	
	//MARK: Calendar
	private let calDataSource = CalDataSource()
	private let calDelegate = CalDelegate()
	public var filterableStore: Filterable?
	private var cancellables = [AnyCancellable]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		setupCalendar()
		bind()
	}
	
	//MARK: Binding
	func bind() {
		filterableStore?.publishedFilterDate
			.sink { if let date = $0 { self.updateCalendar(with: date) } }
		.store(in: &cancellables)
	}
	
	//MARK: Calendar Handling
	func setupCalendar() {
		calDelegate.proxyViewModel = filterableStore
		calendarView.backgroundColor = UIColor(named: "foreground")
		calendarView.ibCalendarDataSource = calDataSource
		calendarView.ibCalendarDelegate = calDelegate
		calendarView.allowsSelection = true
		calendarView.scrollingMode = .stopAtEachSection
		calendarView.showsVerticalScrollIndicator = false
		calendarView.register(CalendarCell.self, forCellWithReuseIdentifier: "dateCell")
		calendarContainer.layer.cornerRadius = 10
		calendarContainer.clipsToBounds = true
	}
	
	
	private func updateCalendar(with date: Date) {
		self.calendarLabel.text = date.titleDescription
		self.calendarView.selectDates([date], triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: false)
		self.calendarView.scrollToDate(date, triggerScrollToDateDelegate: false, animateScroll: false, preferredScrollPosition: .centeredVertically, extraAddedOffset: 0, completionHandler: nil)
	}
}
