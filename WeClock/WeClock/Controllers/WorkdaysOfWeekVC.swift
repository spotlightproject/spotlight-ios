import UIKit

class WorkdaysOfWeekVC: UIViewController, UIInteractionDelegate, UITableViewDataSource {
	
	
	//MARK: IBOutlets
	@IBOutlet weak var doneButton: UXButton!
	@IBOutlet weak var optionsTableView: UITableView!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		doneButton.delegate = self
		optionsTableView.dataSource = self
		optionsTableView.register(CheckableTableViewCell.self, forCellReuseIdentifier: "CheckableCell")
		optionsTableView.sizeToFit()
	}
	
	var destination: String {
		"workLocationPrompt"
	}
	
	var selectedWeekdays: [Weekday] {
		var results = [Weekday]()
		for indexPath in optionsTableView.indexPathsForSelectedRows ?? [] {
			let weekday = tableData[indexPath.row]
			results.append(weekday)
		}
		return results
	}
	
	func buttonTouchUp() {
		var selections = WeekdaySelections()
		selections.select(weekdays: selectedWeekdays)
		Settings.shared.workdaysOfWeek = selections.dictionary
		
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: destination)
		navigationController?.pushViewController(vc, animated: true)
	}
	
	override func viewWillLayoutSubviews() {
		optionsTableView.heightAnchor.constraint(equalToConstant: optionsTableView.contentSize.height).isActive = true
		super.updateViewConstraints()
	}
	
	//MARK: UITableView DataSource
	private var tableData: [Weekday] = [
		.monday, .tuesday, .wednesday, .thursday, .friday, .saturday, .sunday
	]
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		tableData.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "CheckableCell") ?? CheckableTableViewCell()
		cell.textLabel?.text = tableData[indexPath.row].rawValue
		cell.backgroundColor = UIColor(named: "background") ?? .white
		return cell
	}
}

class CheckableTableViewCell: UITableViewCell {
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
    }
}
