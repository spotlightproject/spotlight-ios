import UIKit
import Combine

class BatteryReadingsTodayVC: UIViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var usedLabel: BoldLabel!
	@IBOutlet weak var remainingLabel: BoldLabel!
	@IBOutlet weak var workdayUseLabel: BoldLabel!
	@IBOutlet weak var startingLabel: BoldLabel!
	
	//MARK: IBActions
	@IBAction func leftButtonTapped(_ sender: Any) {
		if let yesterday = viewModel.storedFilterDate?.yesterday {
			viewModel.filter(by: .specificDay(day: yesterday))
		}
	}
	
	@IBAction func rightButtonTapped(_ sender: Any) {
		if let tomorrow = viewModel.storedFilterDate?.tomorrow {
			viewModel.filter(by: .specificDay(day: tomorrow))
		}
	}
	
	//MARK: Properties
	private var cancellables = [AnyCancellable]()
	public var viewModel: Filterable!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		bind()
	}
	
	private func bind() {
		guard let batteryStore = viewModel as? BatteryStore else {
			debug("Couldn't typecast battery store from view model.")
			return
		}
		
		/// Subscribe to convertibles
		batteryStore.convertibles
			.receive(on: DispatchQueue.main)
			.sink {
				let readings = $0 as! [BatteryReading]
				debug($0)
				self.usedLabel.text  = readings.usage.percentString
				self.remainingLabel.text = readings.latestLevel.percentString
				self.startingLabel.text = readings.earliestLevel.percentString
				self.workdayUseLabel.text = readings.workdayUsage?.percentString ?? "--"
		}
		.store(in: &cancellables)
		
		/// Receive updates from calendar
		batteryStore.publishedFilterDate
			.receive(on: DispatchQueue.main)
			.map { $0?.titleDescription ?? "No Date" }
			.sink { self.dateLabel.text = $0}
		.store(in: &cancellables)
	}
}

extension Double {
	
	var percentString: String {
		"\((self * 100).string(roundedTo: .onePlace))%"
	}
}
