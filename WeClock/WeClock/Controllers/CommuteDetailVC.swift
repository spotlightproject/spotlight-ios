import UIKit
import HealthKit
import MapKit

class CommuteDetailVC: UIViewController {
	
	//MARK: IBActions
	@IBAction func deleteButtonTapped(_ sender: Any) {
		deleteWorkout()
		navigationController?.popViewController(animated: true)
	}
	
	//MARK: View Model
	public var workout: HKWorkout!
	private let mapComponent = MapComponent()
	
	//MARK: IBOutlets
	@IBOutlet weak var durationLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var flightsClimbedLabel: UILabel!
	@IBOutlet weak var energyBurnedLabel: UILabel!
	@IBOutlet weak var avgHeartRateLabel: UILabel!
	@IBOutlet weak var mapContainer: UIView!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		mapComponent.snap(to: mapContainer)
		mapComponent.showLoading()
		mapComponent.mapView.delegate = self
		bind()
		makeBold([durationLabel, distanceLabel, flightsClimbedLabel, energyBurnedLabel, avgHeartRateLabel])
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		mapComponent.showMap()
	}
	
	private func bind() {
		durationLabel.text = (workout.duration / 60).string(roundedTo: .noDecimals)
		distanceLabel.text = workout.totalDistance?.doubleValue(for: .meter()).string(roundedTo: .onePlace)
		flightsClimbedLabel.text = workout.totalFlightsClimbed?.doubleValue(for: .count()).string(roundedTo: .noDecimals) ?? "0"
		energyBurnedLabel.text = workout.totalEnergyBurned?.doubleValue(for: .kilocalorie()).string(roundedTo: .onePlace)
		fetchHeartRate(from: workout)
		fetchRoute(from: workout)
	}
	
	private func fetchHeartRate(from workout: HKWorkout) {
		let query = HKModel.Queries.avgHeartRate(during: workout) { (heartRate) in
			DispatchQueue.main.async {
				self.avgHeartRateLabel.text = heartRate.string(roundedTo: .noDecimals)
			}
		}
		HKModel.shared.execute(query)
	}
	
	private func fetchRoute(from workout: HKWorkout) {
		HKModel.Queries.route(from: workout) { (routes) in
			for route in routes {

				HKModel.Queries.locations(from: route) { (locations) in
					DispatchQueue.main.async {
						self.drawRoute(from: locations)
					}
				}.execute()
			}
		}.execute()
	}
	
	//MARK: Route Drawing
	var routeColor: UIColor = .blue
	
	private func drawRoute(from locations: [CLLocation]) {
		//Set color to draw the route
		self.routeColor = UIColor(named: "route-blue") ?? .blue
		
		//Sort locations by timestamp
		let sorted = locations.sorted { (locationA, locationB) -> Bool in
			locationA.timestamp > locationB.timestamp
		}
		
		//Draw route
		let coordinates = sorted.map { $0.coordinate }
		let polyline = MKPolyline(coordinates: coordinates, count: coordinates.count)
		mapComponent.mapView.addOverlay(polyline)
		
		//Add placemarks
		let startAnnotation = MKPointAnnotation()
		startAnnotation.title = "Start"
		startAnnotation.coordinate = coordinates.first!
		self.mapComponent.mapView.addAnnotation(startAnnotation)
		
		let endAnnotation = MKPointAnnotation()
		endAnnotation.title = "End"
		endAnnotation.coordinate = coordinates.last!
		self.mapComponent.mapView.addAnnotation(endAnnotation)
		
		mapComponent.mapView.showAnnotations([startAnnotation, endAnnotation], animated: true)
	}
	
	public func deleteWorkout() {
		HKModel.shared.store.delete(workout) { (success, errorOrNil) in
			if let error = errorOrNil { debug("Deletion error", data: error) }
			if success { debug("Successfully deleted.") }
		}
	}

}

// MARK: - MKMapViewDelegate
extension CommuteDetailVC: MKMapViewDelegate {
	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		let renderer = MKPolylineRenderer(overlay: overlay)
		renderer.strokeColor = routeColor
		renderer.lineWidth = 3.0

		return renderer
	}
}
