import UIKit

class PrivacyInfoVC: UIViewController, UIInteractionDelegate {
	
	//MARK: IBOutlets
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
	}
	
	func buttonTouchUp() {
		showNextPage()
	}
	
	private func showNextPage() {
		if Settings.shared.hasLaunchedPreviously.object {
			showSchedulePage()
		} else {
			showHealthPermissions()
		}
	}
	
	private func showHealthPermissions() {
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "healthPermissions")
		navigationController?.pushViewController(vc, animated: true)
	}
	
	private func showSchedulePage() {
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "onboardingPageThree")
		navigationController?.pushViewController(vc, animated: true)
	}
}
