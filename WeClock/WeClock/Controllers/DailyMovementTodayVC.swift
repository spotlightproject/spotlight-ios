import UIKit
import Combine

class DailyMovementTodayVC: UIViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var stepsTodayLabel: UILabel!
	@IBOutlet weak var walkingDistanceLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var exerciseAmountLabel: BoldLabel!
	@IBOutlet weak var standAmountLabel: BoldLabel!
	
	@IBOutlet weak var exerciseStack: UIStackView!
	@IBOutlet weak var standStack: UIStackView!
	
	//MARK: IBActions
	@IBAction func leftButtonTapped(_ sender: Any) {
		viewModel.filter(by: .specificDay(day: viewModel.storedFilterDate!.yesterday!))
	}
	@IBAction func rightButtonTapped(_ sender: Any) {
		viewModel.filter(by: .specificDay(day: viewModel.storedFilterDate!.tomorrow!))
	}
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		hideForUnpairedWatches() /// Dont't show Watch-required metrics for phone-only users
		bind() /// Bind the view model to the view
	}
	
	//MARK: View Model
	public var viewModel: Filterable!
	private var cancellables = [AnyCancellable]()
	
	private func bind() {
		guard let viewModel = viewModel as? MovementViewModel else {
			debug("Couldn't typecase view model")
			return
		}
		
		viewModel.$steps
			.map(Transforms.DoubleToRoundedString)
			.receive(on: DispatchQueue.main)
			.sink { self.stepsTodayLabel.text = $0 }
		.store(in: &cancellables)
		
		viewModel.$exerciseMinutes
			.map(Transforms.DoubleToRoundedString)
			.receive(on: DispatchQueue.main)
			.sink { self.exerciseAmountLabel.text = $0 }
		.store(in: &cancellables)
		
		viewModel.$standMinutes
			.map(Transforms.DoubleToRoundedString)
			.receive(on: DispatchQueue.main)
			.sink { self.standAmountLabel.text = $0 }
		.store(in: &cancellables)
		
		viewModel.$walkingDistance
			.map { $0.string(roundedTo: .twoPlaces) }
			.receive(on: DispatchQueue.main)
			.sink { self.walkingDistanceLabel.text = "\($0) miles" }
		.store(in: &cancellables)
		
		viewModel.publishedFilterDate
			.receive(on: DispatchQueue.main)
			.map { $0?.titleDescription ?? "No Date" }
			.sink { self.dateLabel.text = $0}
		.store(in: &cancellables)
	}
	
	private func hideForUnpairedWatches() {
		if !Settings.shared.hasPairedAppleWatch.object {
			standStack.isHidden = true
			exerciseStack.isHidden = true
		}
	}
}
