import UIKit
import CoreLocation
import Combine

class SetHomeLocationVC: UIViewController, UIInteractionDelegate {
	
	//MARK: Properties
	public var locationStore = LocationStore()
	private var cancellables = [AnyCancellable]()
	
	//MARK: IBOutlets
	@IBOutlet weak var promptButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		promptButton.delegate = self
		
		locationStore.$placemark
			.map { $0?.location }
			.sink {
				if let location = $0 {
					Settings.shared.homeLocation = location
				}
		}
		.store(in: &cancellables)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		showNavbar()
		if locationStore.placemark != nil {
			showNext()
		}
	}
	
	private func showNavbar() {
		UIView.animate(withDuration: 0.14) {
			self.navigationController?.navigationBar.isHidden = false
		}
	}
	
	func buttonTouchUp() {
		let storyboard = UIStoryboard(name: "Maps", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "locationChooser")
		
		let typed = vc as! MapController
		typed.locationStore = locationStore
		
		navigationController?.pushViewController(typed, animated: true)
	}
	
	func showNext() {
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "onboardingComplete")
		navigationController?.pushViewController(vc, animated: true)
	}
}
