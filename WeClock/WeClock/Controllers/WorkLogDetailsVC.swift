import UIKit
import Combine

class WorkLogDetailsVC: UIViewController, UIInteractionDelegate, UIScrollViewDelegate {
	
	//MARK: IBOutlets
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var textView: UITextView!
	@IBOutlet weak var eventTypeLabel: UILabel!
	@IBOutlet weak var dateRecordedLabel: UILabel!
	@IBOutlet weak var sourceLabel: UILabel!
	@IBOutlet weak var notesCard: UIView!
	@IBOutlet weak var saveButton: UXButton!
	@IBOutlet weak var overworkedRatingLabel: UILabel!
	@IBOutlet weak var unpaidHoursLabel: UILabel!
	
	//MARK: IBActions
	@IBAction func deleteButtonTapped(_ sender: Any) {
		deleteEntry()
	}
	
	@IBAction func editButtonTapped(_ sender: Any) {
		beginEditing()
	}
	
	//MARK: Properties
	public var entry: WorkLogEntry!
	private var cancellables: [AnyCancellable] = []
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		
		/// Button handler
		saveButton.delegate = self
		
		/// Style notes card
		let insetAmount: CGFloat = 12
		textView.layer.cornerRadius = 10
		textView.textContainerInset = UIEdgeInsets(top: insetAmount, left: insetAmount, bottom: insetAmount, right: insetAmount)
		notesCard.addShadows(color: UIColor(named: "shadow") ?? .black, opacity: 0.1, radius: 3)
		notesCard.layer.cornerRadius = 10
		
		/// Keyboard handling
		scrollView.delegate = self
		keyboardManager.subcribeToKeyboardNotifications()
		
		/// Bind view model
		bind()
	}
	
	//MARK: View Model
	private func bind() {
		entry.publisher(for: \.notes)
			.sink { self.textView.text = $0 }
			.store(in: &cancellables)
		entry.publisher(for: \.eventType)
			.sink { self.eventTypeLabel.text = $0 ?? "Unclassified" }
		.store(in: &cancellables)
		entry.publisher(for: \.dateRecorded)
			.sink { self.dateRecordedLabel.text = DateManager.shared.shortString(for: $0) }
		.store(in: &cancellables)
		entry.publisher(for: \.source)
			.sink { self.sourceLabel.text = $0  ?? "iPhone" }
		.store(in: &cancellables)
		entry.publisher(for: \.overworkedRating)
			.map { Double($0).string(roundedTo: .noDecimals) }
			.sink { self.overworkedRatingLabel.text = $0 }
		.store(in: &cancellables)
		entry.publisher(for: \.didWorkUnpaidHours)
			.map { $0 ? "Yes" : "No" }
			.sink { self.unpaidHoursLabel.text = $0 }
			.store(in: &cancellables)
	}
	
	//MARK: Methods
	private func deleteEntry() {
		CoreData.shared.viewContext.delete(self.entry)
		CoreData.shared.saveContext()
		navigationController?.popViewController(animated: true)
	}
	
	//MARK: Entry Editing
	public func beginEditing() {
		UIView.animate(withDuration: 0.2) {
			self.textView.isEditable = true
			self.saveButton.isHidden = false
			self.textView.becomeFirstResponder()
		}
	}
	
	public func endEditing() {
		UIView.animate(withDuration: 0.2) {
			self.textView.resignFirstResponder()
			self.textView.isEditable = false
			self.saveButton.isHidden = true
		}
		entry.notes = textView.text
		entry.saveContext()
	}
	
	//MARK: UIInteraction Delegate
	public func buttonTouchUp() {
		endEditing()
	}
	
	//MARK: Form Management
	private lazy var keyboardManager: KeyboardManager = {
		let km = KeyboardManager(scrollView: self.scrollView)
		km.targetView = self.textView
		return km
	}()
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView.isTracking { textView.resignFirstResponder() }
	}
	
	func textViewDidBeginEditing(_ textView: UITextView) {
		let defaultText = entry.notes
		if textView.text == defaultText { textView.text = String() }
	}
}
