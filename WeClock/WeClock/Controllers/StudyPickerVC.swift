import UIKit
import Combine
import SafariServices
import SwiftUI

class StudyPickerVC: UITableViewController {

	//MARK: IBOutlets
	@IBOutlet weak var editButton: UIBarButtonItem!
	
	//MARK: IBActions
	@IBAction func editButtonTapped(_ sender: Any) {
		viewModel.toggleEditingMode()
	}
	
	@IBAction func optionsButtonTapped(_ sender: Any) {
		showMenu()
	}
	
	//MARK: View Model
	private let viewModel = StudyPickerViewModel()
	private var cancellables = [AnyCancellable]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.dataSource = diffableDataSource
		updateSnapshot(with: viewModel.profiles, animated: false)
		bind()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.useOptedInProfiles()
		viewModel.update()
		normalizeNavbar()
		BatteryReading.takeReading()
	}
	
	private func bind() {
		viewModel.$isEditing
			.map { $0 ? "checkmark.circle" : "pencil" }
			.sink { self.editButton.image = UIImage(systemName: $0) }
		.store(in: &cancellables)
		
		viewModel.$profiles
			.sink { self.updateSnapshot(with: $0, animated: true) }
		.store(in: &cancellables)
	}
	
	private func normalizeNavbar() {
		navigationItem.hidesBackButton = true
		navigationItem.largeTitleDisplayMode = .always
		navigationController?.navigationBar.isTranslucent = true
		navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
		navigationController?.navigationBar.shadowImage = nil
	}
	
	//MARK: UITableView Data Source
	private lazy var diffableDataSource: UXDiffableDataSource<Int, StudyProfile> = {
		UXDiffableDataSource<Int, StudyProfile>(tableView: tableView)
		{ (tableView, indexPath, profile) -> UITableViewCell? in
			let cell = tableView.dequeueReusableCell(withIdentifier: "StudyProfileCell", for: indexPath)
			
			let studyProfileCell = cell as! StudyProfileTableViewCell
			studyProfileCell.tableViewModel = self.viewModel
			studyProfileCell.configure(for: profile)
			studyProfileCell.profile = profile

			return studyProfileCell
		}
	}()
	
	
	private func updateSnapshot(with studies: [StudyProfile], animated: Bool = true) {
		var newSnapshot = NSDiffableDataSourceSnapshot<Int, StudyProfile>()
        newSnapshot.appendSections([0])
		newSnapshot.appendItems(studies, toSection: 0)
		diffableDataSource.apply(newSnapshot, animatingDifferences: animated)
    }
	
	//MARK: UITableView Delegate
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if viewModel.isEditing { return } ///Don't allow navigation while editing
		
		let selectedProfile = viewModel.profiles[indexPath.row]
		
		if selectedProfile.displayName == "Work Log" {
			loadWorkLog()
		} else {
			loadStudyHost(for: selectedProfile)
		}
	}
	
	//MARK: Navigation Routing
	private func loadStudyHost(for selectedProfile: StudyProfile) {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "studyHost") as! StudyHostVC
		vc.studyProfile = selectedProfile
		vc.navigationItem.title = selectedProfile.displayName
		navigationController?.pushViewController(vc, animated: true)
	}
	
	private func loadWorkLog() {
		let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(withIdentifier: "newWorkLog")
		navigationController?.pushViewController(vc, animated: true)
	}
}

//MARK: Swipe Actions
extension StudyPickerVC {
	override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
		
		///Don't allow leading swipes unless editing
		if viewModel.isEditing == false { return UISwipeActionsConfiguration() }
		
		///Don't allows leading swipes on profiles that are already opted in
		let swipedProfile = viewModel.profiles[indexPath.row]
		if swipedProfile.optedIn.object { return UISwipeActionsConfiguration() }
		
		var actions: [UIContextualAction] = []
		let optIn = UIContextualAction(style: .normal, title: "Opt In") { (action, view, completion) in
			self.viewModel.profiles[indexPath.row].optedIn.object = true
			completion(true)
		}
		optIn.backgroundColor = .systemGreen
		
		actions.append(optIn)
		let config = UISwipeActionsConfiguration(actions: actions)
		return config
	}
	
	override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
		
		///Don't allows trailing swipes on profiles that are already opted out
		let swipedProfile = viewModel.profiles[indexPath.row]
		if !swipedProfile.optedIn.object { return UISwipeActionsConfiguration() }
		
		var actions: [UIContextualAction] = []
		let optOut = UIContextualAction(style: .destructive, title: "Opt Out") { (action, view, completion) in
			self.viewModel.profiles[indexPath.row].optedIn.object = false
			if !self.viewModel.isEditing { self.viewModel.useOptedInProfiles() }
			completion(true)
		}
		
		actions.append(optOut)
		let config = UISwipeActionsConfiguration(actions: actions)
		return config
	}
}

//MARK: Menus
extension StudyPickerVC {
	
	public func showMenu() {
		
		let allData = UIAlertAction(title: "All Data", style: .default) { _ in
			let storyboard = UIStoryboard(name: "AllData", bundle: Bundle.main)
			let vc = storyboard.instantiateViewController(identifier: "allData") as! StudyDataAllVC
			vc.convertiblesStore = AllConvertiblesStore(timePeriod: .today)
			vc.navigationItem.title = "All Study Data"
			self.navigationController?.pushViewController(vc, animated: true)
		}
		
		let configure = UIAlertAction(title: "Configure", style: .default) { _ in
			let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
			let vc = storyboard.instantiateViewController(identifier: "onboardingPageOne")
			self.navigationController?.pushViewController(vc, animated: true)
		}
		
		let settings = UIAlertAction(title: "Settings", style: .default) { _ in
			let hc = UIHostingController(rootView: SettingsView())
			hc.view.backgroundColor = UIColor(named: "background")!
			self.navigationController?.pushViewController(hc, animated: true)
		}
		
		/// Safari Links
		let config = SFSafariViewController.Configuration()
		config.entersReaderIfAvailable = true
		
		let privacyPolicy = UIAlertAction(title: "Privacy Policy", style: .default) { _ in
			let url = URL(string: "http://spotlightproject.gitlab.io/privacy.html")!
			let sc = SFSafariViewController(url: url, configuration: config)
			self.present(sc, animated: true, completion: nil)
		}
		
		let aboutUs = UIAlertAction(title: "About", style: .default) { _ in
			let url = URL(string: "https://spotlightproject.gitlab.io")!
			let sc = SFSafariViewController(url: url, configuration: config)
			self.present(sc, animated: true, completion: nil)
		}
		
		let ac = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
		ac.addAction(allData)
		ac.addAction(configure)
		ac.addAction(privacyPolicy)
		ac.addAction(aboutUs)
		ac.addAction(settings)
		ac.addAction(UIAlertAction.cancelAction)
		
		present(ac, animated: true, completion: nil)
	}
}
