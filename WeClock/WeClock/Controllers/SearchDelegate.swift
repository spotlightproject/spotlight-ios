import UIKit

class SearchDelegate: NSObject, UISearchBarDelegate {
	
	//MARK: Properties
	private let study: Searchable & StudyConvertiblesStore
	
	//MARK: Initialization
	internal init(study: Searchable & StudyConvertiblesStore) {
		self.study = study
	}
	
	//MARK: UISearchBar Delegate
	public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText == "" {
			study.reset()
		} else {
			study.search(for: searchText)
		}
	}
	
	public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
	}
}
