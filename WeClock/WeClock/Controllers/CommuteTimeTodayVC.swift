import UIKit
import Combine
import MapKit

class CommuteTimeTodayVC: UIViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var mapContainer: UIView!
	@IBOutlet weak var durationLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var energyLabel: UILabel!
	@IBOutlet weak var flightsClimbedLabel: UILabel!
	@IBOutlet weak var avgHeartRateLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	
	//MARK: IBActions
	@IBAction func leftButtonTapped(_ sender: Any) {
		viewModel.filter(by: .specificDay(day: viewModel.storedFilterDate!.yesterday!))
	}
	
	@IBAction func rightButtonTapped(_ sender: Any) {
		viewModel.filter(by: .specificDay(day: viewModel.storedFilterDate!.tomorrow!))
	}
	//MARK: View Models
	public var viewModel: Filterable!
	
	//MARK: Views
	let mapComponent = MapComponent()
	
	//MARK: Properties
	private var cancellables = [AnyCancellable]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		viewModel.filter(by: .today)
		mapComponent.snap(to: mapContainer)
		mapComponent.showLoading()
		makeBold([durationLabel, distanceLabel, energyLabel, flightsClimbedLabel, avgHeartRateLabel])
		bind()
		mapComponent.mapView.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		mapComponent.showMap()
	}
	
	//MARK: Binding
	public func bind() {
		guard let commuteStore = viewModel as? CommuteStore else {
			debug("Couldn't typecast commute store.")
			return
		}
		
		commuteStore.$totalDuration
			.receive(on: DispatchQueue.main)
			.map { ($0 / 60).string(roundedTo: .noDecimals) }
			.sink { self.durationLabel.text = "\($0)" }
		.store(in: &cancellables)
		commuteStore.$totalDistance
			.receive(on: DispatchQueue.main)
			.map { $0.string(roundedTo: .onePlace) }
			.sink { self.distanceLabel.text = "\($0)" }
		.store(in: &cancellables)
		commuteStore.$totalEnergy
			.receive(on: DispatchQueue.main)
			.map { $0.string(roundedTo: .twoPlaces) }
			.sink { self.energyLabel.text = "\($0)" }
		.store(in: &cancellables)
		commuteStore.$totalFlightsClimbed
			.receive(on: DispatchQueue.main)
			.map { $0.string(roundedTo: .noDecimals) }
			.sink { self.flightsClimbedLabel.text = $0 }
		.store(in: &cancellables)
		commuteStore.$averageBPM
			.receive(on: DispatchQueue.main)
			.map { $0.string(roundedTo: .noDecimals) }
			.sink { self.avgHeartRateLabel.text = $0 }
		.store(in: &cancellables)
		commuteStore.routesSubject
			.receive(on: DispatchQueue.main)
			.sink { if !$0.isEmpty { self.drawRoute(from: $0) } }
		.store(in: &cancellables)
		commuteStore.publishedFilterDate
			.receive(on: DispatchQueue.main)
			.map { $0?.titleDescription ?? "No Date" }
			.sink { self.dateLabel.text = $0}
		.store(in: &cancellables)
	}
	
	//MARK: Route Drawing
	let routeColors = RouteColorManager()
	
	private func drawRoute(from locations: [CLLocation]) {
		//Sort locations by timestamp
		let sorted = locations.sorted { (locationA, locationB) -> Bool in
			locationA.timestamp > locationB.timestamp
		}
		
		//Draw route
		let coordinates = sorted.map { $0.coordinate }
		let polyline = MKPolyline(coordinates: coordinates, count: coordinates.count)
		mapComponent.mapView.addOverlay(polyline)
		
		//Add placemarks
		let startAnnotation = MKPointAnnotation()
		startAnnotation.title = "Start"
		startAnnotation.coordinate = coordinates.first!
		self.mapComponent.mapView.addAnnotation(startAnnotation)
		
		let endAnnotation = MKPointAnnotation()
		endAnnotation.title = "End"
		endAnnotation.coordinate = coordinates.last!
		self.mapComponent.mapView.addAnnotation(endAnnotation)
		
		mapComponent.mapView.showAnnotations([startAnnotation, endAnnotation], animated: true)
	}
}

// MARK: - MKMapViewDelegate
extension CommuteTimeTodayVC: MKMapViewDelegate {
	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		let renderer = MKPolylineRenderer(overlay: overlay)
		renderer.strokeColor = routeColors.color()
		renderer.lineWidth = 3.0

		return renderer
	}
}

class RouteColorManager {
	private var colors: [UIColor] = [
		UIColor(named: "route-blue") ?? .blue,
		UIColor(named: "route-purple") ?? .purple,
		.yellow,
		.orange
	]
	
	private var index = 0
	
	private var currentIndex: Int {
		let cache = index
		if index >= (colors.count - 1) {
			index = 0
		} else {
			index += 1
		}
		return cache
	}
	
	public func color() -> UIColor {
		colors[currentIndex]
	}
}
