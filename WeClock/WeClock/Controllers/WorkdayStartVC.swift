import UIKit

class WorkdayStartVC: UIViewController, UIInteractionDelegate {
	
	//MARK: IBOutlets
	@IBOutlet weak var nextButton: UXButton!
	@IBOutlet weak var pickerView: UIDatePicker!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
	}
	
	var destination: String {
		"workdayEndChooser"
	}
	
	func buttonTouchUp() {
		//Save start time
		Settings.shared.workdayStartTime = pickerView.date
		
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: destination)
		navigationController?.pushViewController(vc, animated: true)
	}
}
