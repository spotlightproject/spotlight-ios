import UIKit

class WorkdayEndVC: UIViewController, UIInteractionDelegate {
	
	//MARK: IBOutlets
	@IBOutlet weak var pickerView: UIDatePicker!
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
	}
	
	var destination: String {
		"workdaysOfWeek"
	}
	
	func buttonTouchUp() {
		Settings.shared.workdayEndTime = pickerView.date
		
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: destination)
		navigationController?.pushViewController(vc, animated: true)
	}
}
