import UIKit
import SwiftUI

struct SettingsView: View {
	
	//MARK: Data Sources
	@State var startTime = Settings.shared.workdayStartTime
	@State var endTime = Settings.shared.workdayEndTime
	
	//MARK: Properties
	var shortFormatter: DateFormatter {
		let formatter = DateFormatter()
		formatter.dateStyle = .none
		formatter.timeStyle = .short
		return formatter
	}
	
	var startTimeString: String {
		guard let date = startTime else {
			return String()
		}
		return shortFormatter.string(from: date)
	}
	
	var endTimeString: String {
		guard let date = endTime else {
			return String()
		}
		return shortFormatter.string(from: date)
	}
	
	
	//MARK: Settings
	private var locationTracking = Settings.shared.locationsOptedIn
	private var healthAccess = Settings.shared.allHealthDataOptedIn
	private var batteryMonitoring = Settings.shared.batteryLevelsOptedIn
	
	//MARK: Initialization
	init() {
		UITableView.appearance().backgroundColor = UIColor(named: "background") ?? .systemBackground
	}
	
	var body: some View {
		Form {
			Section(header: Text("Work Day")) {
				NavigationLink(destination: WorkdayStartPicker()) {
					Text("Starts at \(startTimeString)")
				}
				NavigationLink(destination: WorkdayEndPicker()) {
					Text("Ends at \(endTimeString)")
				}
			}
			
			Section(header: Text("Data Privacy")) {
				Toggle(isOn: locationTracking.binding) {
					Text("Location Tracking Enabled")
				}
				Toggle(isOn: healthAccess.binding) {
					Text("Health Access Granted")
				}
				Toggle(isOn: batteryMonitoring.binding) {
					Text("Batter Monitoring")
				}
			}
		}
		.onAppear {
			self.startTime = Settings.shared.workdayStartTime
			self.endTime = Settings.shared.workdayEndTime
		}
		.navigationBarTitle("Settings")
	}
}
