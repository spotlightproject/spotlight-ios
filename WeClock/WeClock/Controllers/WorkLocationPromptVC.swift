import UIKit

class WorkLocationPromptVC: UIViewController, UIInteractionDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
	
	//MARK: IBOutlets
	@IBOutlet weak var pickerView: UIPickerView!
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
		pickerView.dataSource = self
		pickerView.delegate = self
	}
	
	//MARK: UIPickerView Data Source
	var pickerData = ["Yes, I do!", "No, it varies."]
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return pickerData.count
	}
	
	//MARK: UIPickerView Delegate
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		pickerData[row]
	}
	
	var destination: String {
		switch pickerView.selectedRow(inComponent: 0) {
			case 0: return "onboardingPageFour"
			case 1: return "onboardingComplete"
			default: return "onboardingPageFour"
		}
	}
	
	func buttonTouchUp() {
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: destination)
		navigationController?.pushViewController(vc, animated: true)
	}
	
}
