import UIKit
import CoreLocation

class LocationPermissionsVC: UIViewController, UIInteractionDelegate {
	
	//MARK: Properties
	var locationManager = CLLocationManager()
	
	//MARK: IBOutlets
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}
	
	func buttonTouchUp() {
		locationManager.requestAlwaysAuthorization()
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "onboardingPageThree")
		navigationController?.pushViewController(vc, animated: true)
	}
}
