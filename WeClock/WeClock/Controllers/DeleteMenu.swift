import UIKit

class DeleteMenu {
	
	let viewModel: Deletable
	
	init(viewModel: Deletable) {
		self.viewModel = viewModel
	}
	
	public var deleteController: UIAlertController {
		let delete = UIAlertAction(title: "Delete All", style: .destructive) { (_) in
			self.viewModel.delete()
		}
		
		let ac = UIAlertController(title: "Delete", message: "Are you sure you want to delete all work logs? This cannot be undone.", preferredStyle: .actionSheet)
		ac.addAction(delete)
		ac.addAction(UIAlertAction.cancelAction)
		
		return ac
	}
}
