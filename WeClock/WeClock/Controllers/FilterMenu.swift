import UIKit

class FilterMenu<VM: StudyStore> {
	
	let viewModel: VM
	
	init(viewModel: VM) {
		self.viewModel = viewModel
	}
	
	public var menuController: UIAlertController {
		let today = UIAlertAction(title: "Today", style: .default) { (_) in
			self.viewModel.filter(by: .today)
		}
		let thisWeek = UIAlertAction(title: "This Week", style: .default) { (_) in
			self.viewModel.filter(by: .thisWeek)
		}
		let thisMonth = UIAlertAction(title: "This Month", style: .default) { (_) in
			self.viewModel.filter(by: .thisMonth)
		}
		let allTime = UIAlertAction(title: "All Time", style: .default) { (_) in
			self.viewModel.unfilter()
		}

		let ac = UIAlertController(title: "Filter", message: "Choose a time period to filter work log entries.", preferredStyle: .actionSheet)
		ac.addAction(today)
		ac.addAction(thisWeek)
		ac.addAction(thisMonth)
		ac.addAction(allTime)
		ac.addAction(UIAlertAction.cancelAction)
		
		return ac
	}
	
}
