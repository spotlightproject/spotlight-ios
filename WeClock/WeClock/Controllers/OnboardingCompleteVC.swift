import UIKit

class OnboardingCompleteVC: UIViewController, UIInteractionDelegate{
	
	//MARK: IBOutlets
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
		self.navigationItem.hidesBackButton = true
	}
	
	func buttonTouchUp() {
		if Settings.shared.hasLaunchedPreviously.object {
			popToStudyPicker()
		} else {
			Settings.shared.hasLaunchedPreviously.object = true
			pushToStudyPicker()
		}
	}
	
	private func pushToStudyPicker() {
		let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "studyPicker")
		navigationController?.setViewControllers([vc], animated: true)
	}
	
	private func popToStudyPicker() {
		navigationController?.popToRootViewController(animated: true)
	}
}
