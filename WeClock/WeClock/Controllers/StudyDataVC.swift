import UIKit
import Combine

class StudyDataVC: UITableViewController {

	//MARK: IBOutlets
	@IBOutlet weak var filterButton: UIBarButtonItem!
	@IBOutlet weak var searchBar: UISearchBar!
	
	//MARK: IBActions
	@IBAction func shareButtonTapped(_ sender: Any) {
		let studyResults = threadSafeCache.map { $0.studyResult }
		studyResults.export(from: self)
	}
	
	@IBAction func filterButtonTapped(_ sender: Any) {
		showFilterMenu()
	}
	
	@IBAction func optionsButtonTapped(_ sender: Any) {
		showOptionsMenu()
	}
	
	//MARK: Menus and Delegates
	private lazy var searchDelegate: SearchDelegate? = {
		if let searchable = viewModel as? Searchable & StudyConvertiblesStore {
			return SearchDelegate(study: searchable)
		} else {
			return nil
		}
	}()
	
	//MARK: Data Sources
	public var viewModel: StudyStore!
	public var studyProfile: StudyProfile!
	private var cancellables = [AnyCancellable]()
	
	/// Use the thread safe cache if you need to access study convertibles imperatively from the main thread. Otherwise subscribe to `viewModel.convertibles` and receive them on the appropriate thread.
	private var threadSafeCache = [StudyResultConvertible]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.largeTitleDisplayMode = .always

		///Delegates and data sources
		tableView.dataSource = diffableDataSource
		configureSearchBar()
		bind()
	}
	
	public func bind() {
		///Bind table, cache table data
		viewModel.convertibles
			.receive(on: DispatchQueue.main)
			.sink {
				self.updateSnapshot(with: self.transformToStudyResults($0))
				self.threadSafeCache = $0
			}
		.store(in: &cancellables)
		
		///Bind filter icon
		viewModel.filtering
			.sink { self.setFilterIcon(isFiltering: $0) }
		.store(in: &cancellables)
	}
	
	//MARK: Transforms
	var transformToStudyResults: ([StudyResultConvertible]) -> [StudyResult] = {
		$0.map { $0.studyResult }
	}
	
	//MARK: Search Bar Management
	private func configureSearchBar() {
		if viewModel as? Searchable != nil { ///hide the search bar for non-searchable studies
			searchBar.delegate = searchDelegate
		} else {
			tableView.tableHeaderView?.frame = .zero
		}
	}
	
	//MARK: UITableView Data Source
	private lazy var diffableDataSource: UITableViewDiffableDataSource<Int, StudyResult> = {
		UITableViewDiffableDataSource<Int, StudyResult>(tableView: tableView)
		{ (tableView, indexPath, result) -> UITableViewCell? in
			let cell = tableView.dequeueReusableCell(withIdentifier: "studyDataCell", for: indexPath)
			
			cell.textLabel?.text = result.content
			cell.detailTextLabel?.text = DateManager.shared.shortString(for: result.date)
			
			return cell
		}
	}()
	
	private func updateSnapshot(with samples: [StudyResult], animated: Bool = true) {
        var newSnapshot = NSDiffableDataSourceSnapshot<Int, StudyResult>()
        newSnapshot.appendSections([0])
		newSnapshot.appendItems(samples, toSection: 0)
		diffableDataSource.apply(newSnapshot, animatingDifferences: animated)
    }
	
	//MARK: UITableView Delegate
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedConvertible = threadSafeCache[indexPath.row]
		navigationController?.pushViewController(selectedConvertible.detailController, animated: true)
	}
	
	//MARK: Icon Management
	public func setFilterIcon(isFiltering: Bool) {
		UIView.animate(withDuration: 0.15) {
			let imageName = isFiltering ? "line.horizontal.3.decrease.circle.fill" : "line.horizontal.3.decrease.circle"
			self.filterButton.image = UIImage(systemName: imageName)!
		}
	}
}


//MARK: Filter Menu
extension StudyDataVC {
	
	private func showOptionsMenu() {
		
		let ac = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
		
		let optOut = UIAlertAction(title: "Opt Out", style: .default) { (_) in
			self.studyProfile.optedIn.object = false
			self.navigationController?.popToRootViewController(animated: true)
		}

		if let deletable = viewModel as? Deletable {
			let deleteAll = UIAlertAction(title: "Delete All Data", style: .destructive) { (_) in
				deletable.delete()
			}
			ac.addAction(deleteAll)
		}
		
		ac.addAction(optOut)
		ac.addAction(UIAlertAction.cancelAction)
		
		present(ac, animated: true, completion: nil)
	}
	
	private func showFilterMenu() {
		let today = UIAlertAction(title: "Today", style: .default) { (_) in
			self.viewModel.filter(by: .today)
		}
		let thisWeek = UIAlertAction(title: "This Week", style: .default) { (_) in
			self.viewModel.filter(by: .thisWeek)
		}
		let thisMonth = UIAlertAction(title: "This Month", style: .default) { (_) in
			self.viewModel.filter(by: .thisMonth)
		}
		let allTime = UIAlertAction(title: "All Time", style: .default) { (_) in
			self.viewModel.unfilter()
		}

		let ac = UIAlertController(title: "Filter", message: "Choose a time period to filter results by.", preferredStyle: .actionSheet)
		ac.addAction(today)
		ac.addAction(thisWeek)
		ac.addAction(thisMonth)
		ac.addAction(allTime)
		ac.addAction(UIAlertAction.cancelAction)
		present(ac, animated: true, completion: nil)
	}
}
