import UIKit
import CoreLocation
import Combine

class SetWorkLocationVC: UIViewController, UIInteractionDelegate {
	
	//MARK: IBOutlets
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: Properties
	private let locationStore = LocationStore()
	private var cancellables = [AnyCancellable]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
		
		locationStore.$placemark
			.map { $0?.location }
			.sink {
				if let location = $0 {
					print("Received location")
					Settings.shared.workLocation = location
				}
		}
		.store(in: &cancellables)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		showNavbar()
		super.viewDidAppear(animated)
		if locationStore.placemark != nil {
			showNext()
		}
	}
	
	private func showNavbar() {
		UIView.animate(withDuration: 0.14) {
			self.navigationController?.navigationBar.isHidden = false
		}
	}
	
	func buttonTouchUp() {
		let storyboard = UIStoryboard(name: "Maps", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "locationChooser")
		
		let typed = vc as! MapController
		typed.locationStore = locationStore
		
		navigationController?.pushViewController(typed, animated: true)
	}
	
	private func showNext() {
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "setHomeLocation")
		navigationController?.pushViewController(vc, animated: true)
	}
}
