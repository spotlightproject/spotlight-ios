import Foundation
import MapKit
import UIKit
import Combine

class MapController: UIViewController, MKMapViewDelegate, UISearchBarDelegate, UITableViewDelegate {
	
	//MARK: Properties
	public let mapView = MKMapView()
	private var cachedItems = [MKMapItem]()
	private var selectedPlacemark: MKPlacemark?
	public var locationStore: LocationStore!
	
	//MARK: IBActions
	@IBAction func cancelButtonTapped(_ sender: Any) {
		navigationController?.popViewController(animated: true)
	}
	
	@IBAction func chooseButtonTapped(_ sender: Any) {
		navigationController?.popViewController(animated: true)
		locationStore.placemark = selectedPlacemark
	}
	
	@IBAction func closeButtonTapped(_ sender: Any) {
		showHalfSearchCard()
		hideResultCard()
	}
	
	//MARK: IBOutlets
	@IBOutlet weak var resultsCardPanIndicator: UIView!
	@IBOutlet weak var searchCardPanIndicator: UIView!
	@IBOutlet weak var resultCard: UIView!
	@IBOutlet weak var cancelButton: UIButton!
	@IBOutlet weak var cardHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var resultCardHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var searchCard: UIView!
	@IBOutlet weak var searchTableView: UITableView!
	@IBOutlet weak var stackView: UIStackView!
	@IBOutlet weak var chooseButton: UIButton!
	@IBOutlet weak var primaryLabel: UILabel!
	@IBOutlet weak var secondaryLabel: UILabel!
	@IBOutlet weak var cancelCard: BlurredCard!
	
	var resultsCardContentHeight: CGFloat {
		stackView.frame.size.height + 50 + 30
	}
	
	var searchCardMinimumContentHeight: CGFloat {
		30
	}
	
	let placeholderLabel = UILabel()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		
		///Construct map view
		mapView.frame = view.bounds
		mapView.delegate = self
		view.insertSubview(mapView, at: 0)
		
		/// Hide navigation bar
		navigationItem.hidesBackButton = true
		navigationController?.navigationBar.isHidden = true
		
		searchBar.delegate = self
		primaryLabel.embolden()
		searchTableView.dataSource = diffableDataSource
		searchTableView.delegate = self
		
		/// Button corners
		chooseButton.layer.cornerRadius = 12
		cancelButton.layer.cornerRadius = 12
		searchCardPanIndicator.layer.cornerRadius = 3
		resultsCardPanIndicator.layer.cornerRadius = 3
		
		searchCard.addShadows(color: .black, opacity: 0.2, radius: 5)
		resultCard.addShadows(color: .black, opacity: 0.2, radius: 5)
		cancelCard.addShadows(color: .black, opacity: 0.1, radius: 1)
		cancelCard.cornerRadius = 7
		cancelCard.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
		
		/// Handle taps
		let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
		mapView.addGestureRecognizer(tapRecognizer)
		
		/// Handle pans
		let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(gestureReceieved(_:)))
		searchCard.addGestureRecognizer(gestureRecognizer)
		
		let resultsGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(resultsCardPanReceived(_:)))
		resultCard.addGestureRecognizer(resultsGestureRecognizer)
		
		///Set up placeholder label
		searchTableView.addSubview(placeholderLabel)
		placeholderLabel.text = "No results."
		placeholderLabel.font = UIFont.preferredFont(forTextStyle: .title2)
		placeholderLabel.embolden()
		placeholderLabel.textColor = UIColor.tertiaryLabel
		placeholderLabel.isHidden = true
		placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
		placeholderLabel.centerXAnchor.constraint(equalTo: searchCard.centerXAnchor).isActive = true
		placeholderLabel.centerYAnchor.constraint(equalTo: searchCard.centerYAnchor).isActive = true
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		mapView.showsUserLocation = true
	}
	
	//MARK: MKMapView Delegate
	var userNotYetLocated = true
	func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
		print("User location: \(userLocation)")
		if userNotYetLocated {
			let userRegion = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
			mapView.region = userRegion
			userNotYetLocated = false
		}
	}
	
	func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
		print("Error locating user: \(error)")
	}
	
	//MARK: UISearchBar Delegate
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		search(for: searchBar.text ?? "")
		searchBar.resignFirstResponder()
	}
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		showFullSearchCard()
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		showFullSearchCard()
	}
	
	//MARK: Gestures
	var searchCardStartingYConstant: CGFloat = 0
	var searchCardState: CardState = .min
	var resultsCardStartingYConstant: CGFloat = 0
	
	@objc private func gestureReceieved(_ pan: UIPanGestureRecognizer) {
		searchBar.resignFirstResponder()
		let translation = pan.translation(in: view)
		
		switch pan.state {
			case .began: searchCardStartingYConstant = cardHeightConstraint.constant
			case .changed:
				self.cardHeightConstraint.constant = self.searchCardStartingYConstant - translation.y
				self.view.layoutIfNeeded()
			case .ended:
				if translation.y > 0 {
					animateSearchCardDown()
				} else {
					self.showFullSearchCard()
				}
			default: return
		}
	}
	
	@objc private func resultsCardPanReceived(_ pan: UIPanGestureRecognizer) {
		searchBar.resignFirstResponder()
		let translation = pan.translation(in: view)
		
		switch pan.state {
			case .began:
				resultsCardStartingYConstant = resultCardHeightConstraint.constant
			case .changed:
				self.resultCardHeightConstraint.constant = self.resultsCardStartingYConstant - translation.y
				self.view.layoutIfNeeded()
			case .ended:
				if translation.y > 0 {
					self.hideResultCard()
					self.showHalfSearchCard()
				} else {
					self.showResultCard()
				}
			default: return
		}
	}
	
	//MARK: Animations
	private func animateSearchCardDown() {
		switch searchCardState {
			case .full: showHalfSearchCard()
			default: showMinimumSearchCard()
		}
	}
	
	private func showResultCard() {
		if let selectedIndexPath = searchTableView.indexPathForSelectedRow {
			searchTableView.deselectRow(at: selectedIndexPath, animated: true)
		}
		UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
			self.resultCardHeightConstraint.constant = self.resultsCardContentHeight
			self.view.layoutIfNeeded()
		}, completion: nil)
	}
	
	private func hideResultCard() {
		UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
			self.resultCardHeightConstraint.constant = 0
			self.view.layoutIfNeeded()
		}, completion: nil)
	}
	
	private func showMinimumSearchCard() {
		UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
			self.cardHeightConstraint.constant = 30
			self.view.layoutIfNeeded()
		}, completion: nil)
		searchCardState = .min
	}
	
	private func showHalfSearchCard() {
		UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
			self.cardHeightConstraint.constant = 250
			self.view.layoutIfNeeded()
		}, completion: nil)
		searchCardState = .half
	}
	
	private func showFullSearchCard() {
		UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
			self.cardHeightConstraint.constant = 450
			self.view.layoutIfNeeded()
		}, completion: nil)
		searchCardState = .full
	}
	
	private func animatePlacholder(isHidden: Bool) {
		UIView.animate(withDuration: 0.15) {
			self.placeholderLabel.isHidden = isHidden
		}
	}
	
	private func search(for query: String) {
		let request = MKLocalSearch.Request()
		request.naturalLanguageQuery = query
		request.region = mapView.region
		
		MKLocalSearch(request: request).start { (response, error) in
			if let error = error { print("Error: \(error)"); self.update(with: []) }
			if let response = response {
				self.animatePlacholder(isHidden: !response.mapItems.isEmpty)
				self.update(with: response.mapItems)
			} else {
				self.animatePlacholder(isHidden: false)
			}
		}
	}
	
	//MARK: UITableView Data Source
	public lazy var diffableDataSource = {
		UITableViewDiffableDataSource<Int, MKMapItem>(tableView: searchTableView) { (tableView, indexPath, mapItem) -> UITableViewCell? in
			let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell") ?? UITableViewCell()
			cell.textLabel?.text = mapItem.placemark.name
			cell.detailTextLabel?.text = mapItem.placemark.title
			return cell
		}
	}()
	
	private func update(with mapItems: [MKMapItem]) {
		cachedItems = mapItems
		var newSnapshot = NSDiffableDataSourceSnapshot<Int, MKMapItem>()
		newSnapshot.appendSections([0])
		newSnapshot.appendItems(mapItems)
		self.diffableDataSource.apply(newSnapshot, animatingDifferences: true, completion: nil)
	}
	
	//MARK: UITableView Delegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		searchBar.resignFirstResponder()
		
		///Configure UI for selected map item
		let selectedMapItem = cachedItems[indexPath.row]
		//update(with: [selectedMapItem])
		addAnnotation(for: selectedMapItem)
		configureResultsCard(for: selectedMapItem)
		selectedPlacemark = selectedMapItem.placemark
		
		///Animate card changes
		showMinimumSearchCard()
		showResultCard()
	}
	
	private func configureResultsCard(for mapItem: MKMapItem) {
		primaryLabel.text = mapItem.placemark.name
		secondaryLabel.text = mapItem.placemark.title
	}
	
	private func addAnnotation(for mapItem: MKMapItem) {
		mapView.removeAnnotations(mapView.annotations)
		let regionSpan: CLLocationDistance = 200
		
		let annotation = MKPointAnnotation()
		annotation.coordinate = mapItem.placemark.coordinate
		annotation.title = mapItem.placemark.name
		let newRegion = MKCoordinateRegion(
			center: mapItem.placemark.coordinate,
			latitudinalMeters: regionSpan,
			longitudinalMeters: regionSpan
		)
		mapView.setRegion(newRegion, animated: true)
		mapView.addAnnotation(annotation)
	}
	
	//MARK: Pin Dropping
	@objc func handleTap(gestureRecognizer: UILongPressGestureRecognizer) {
		let mapCoordinates = gestureRecognizer.location(in: mapView)
		let coordinate = mapView.convert(mapCoordinates, toCoordinateFrom: mapView)
		
		//Remove previous annotation
		mapView.removeAnnotations(mapView.annotations)
		
		//Get placemark info and add annotation (also cache it in `previousAnnotation` for deletion later)
		let clLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
		dropPin(at: clLocation)
	}
	
	func dropPin(at location: CLLocation) {
		//Set up new annotation with tap coordinates
		let annotation = MKPointAnnotation()
		annotation.coordinate = location.coordinate
		
		//Get placemark info and add annotation (also cache it in `previousAnnotation` for deletion later)
		location.placemark { (placemark) in
			annotation.title = placemark.name
			self.mapView.addAnnotation(annotation)
			let newPlacemark = MKPlacemark(placemark: placemark)
			self.selectedPlacemark = newPlacemark
			let newMapItem = MKMapItem(placemark: newPlacemark)
			self.configureResultsCard(for: newMapItem)
			self.showResultCard()
		}
	}
}

enum CardState {
	case full, half, min
}

class LocationStore: ObservableObject {
	@Published public var placemark: MKPlacemark?
}
