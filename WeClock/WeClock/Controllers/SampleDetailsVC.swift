import UIKit
import HealthKit

class HKQuantitySampleDetailsVC: UIViewController {
	
	//MARK: Properties
	var sample: HKQuantitySample!
	
	//MARK: IBOutletsg
	@IBOutlet weak var quantityValueLabel: UILabel!
	@IBOutlet weak var unitLabel: UILabel!
	@IBOutlet weak var sourceLabel: UILabel!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		makeBold(quantityValueLabel)
		
		guard let unit = sample.friendlyUnit else {
			debug("No friendly unit for sample")
			return
		}
		
		quantityValueLabel.text = sample.quantity.doubleValue(for: unit).string(roundedTo: .noDecimals)
		unitLabel.text = unit.description
		sourceLabel.text = sample.sourceRevision.productType
		
	}
	
}
