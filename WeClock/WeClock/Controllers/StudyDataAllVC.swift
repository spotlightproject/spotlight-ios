import UIKit
import Combine

class StudyDataAllVC: UITableViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var segmentedControl: UISegmentedControl!
	@IBOutlet weak var deleteButton: UIBarButtonItem!
	@IBOutlet weak var filterButton: UIBarButtonItem!
	
	//MARK: IBActions
	@IBAction func shareButtonTapped(_ sender: Any) {
		let results = threadSafeCache.map { $0.studyResult }
		results.export(from: self)
	}
	@IBAction func filterButtonTapped(_ sender: Any) {
		present(filterMenu.menuController, animated: true, completion: nil)
	}
	@IBAction func deleteButtonTapped(_ sender: Any) {
		present(deleteMenu.deleteController, animated: true, completion: nil)
	}

	@IBAction func segmentedControlDidChange(_ sender: Any) {
		if segmentedControl.selectedSegmentIndex == 0 {
			convertiblesStore.useSpotlightData()
		} else {
			convertiblesStore.useHealthData()
		}
	}
	
	//MARK: Data Sources
	public var convertiblesStore: AllConvertiblesStore!
	
	//MARK: Properties
	private var cancellables = [AnyCancellable]()
	private lazy var filterMenu: FilterMenu = {
		FilterMenu(viewModel: convertiblesStore)
	}()
	
	private lazy var deleteMenu: DeleteMenu = {
		DeleteMenu(viewModel: convertiblesStore)
	}()
	
	/// Use the thread safe cache if you need to access study convertibles imperatively from the main thread. Otherwise subscribe
	///  to `viewModel.convertibles` and receive them on the appropriate thread.
	private var threadSafeCache = [StudyResultConvertible]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.dataSource = diffableDataSource
		
		bind()
	}
	
	private func bind() {
		/// Bind convertibles
		convertiblesStore.convertibles
			.map(sortConvertibles)
			.receive(on: DispatchQueue.main)
			.sink {
				self.updateSnapshot(with: self.transformToStudyResults($0))
				self.threadSafeCache = $0
			}
		.store(in: &cancellables)
		
		/// Bind filter icon
		convertiblesStore.filtering
			.sink { self.setFilterIcon(isFiltering: $0) }
		.store(in: &cancellables)
		
		/// Bind delete icon
		convertiblesStore.$isDeletable
			.sink { self.setDeleteIcon(isDeletable: $0) }
		.store(in: &cancellables)
	}
	
	//MARK: Transforms
	var transformToStudyResults: ([StudyResultConvertible]) -> [StudyResult] = {
		$0.map { $0.studyResult }
	}

	var sortConvertibles: ([StudyResultConvertible]) -> [StudyResultConvertible] = {
		return $0.sorted { (a, b) -> Bool in
			guard let a = a.studyResultDate, let b = b.studyResultDate else { return false }
			return a > b
		}
	}
	
	//MARK: UITableView Data Source
	private lazy var diffableDataSource: UITableViewDiffableDataSource<Int, StudyResult> = {
		UITableViewDiffableDataSource<Int, StudyResult>(tableView: tableView)
		{ (tableView, indexPath, result) -> UITableViewCell? in
			let cell = tableView.dequeueReusableCell(withIdentifier: "studyDataCell", for: indexPath)
			
			cell.textLabel?.text = result.content
			cell.detailTextLabel?.text = DateManager.shared.shortString(for: result.date)
			
			return cell
		}
	}()
	
	private func updateSnapshot(with results: [StudyResult], animated: Bool = true) {
        var newSnapshot = NSDiffableDataSourceSnapshot<Int, StudyResult>()
        newSnapshot.appendSections([0])
		newSnapshot.appendItems(results, toSection: 0)
		diffableDataSource.apply(newSnapshot, animatingDifferences: animated)
    }
	
	//MARK: UITableView Delegate
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedConvertible = threadSafeCache[indexPath.row]
		navigationController?.pushViewController(selectedConvertible.detailController, animated: true)
	}
	
	//MARK: Icon Management
	public func setFilterIcon(isFiltering: Bool) {
		UIView.animate(withDuration: 0.15) {
			let imageName = isFiltering ? "line.horizontal.3.decrease.circle.fill" : "line.horizontal.3.decrease.circle"
			self.filterButton.image = UIImage(systemName: imageName)!
		}
	}
	
	public func setDeleteIcon(isDeletable: Bool) {
		UIView.animate(withDuration: 0.15) {
			self.deleteButton.isEnabled = isDeletable
		}
	}
	
}
