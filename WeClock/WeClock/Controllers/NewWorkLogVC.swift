import UIKit

class NewWorkLogVC: UIViewController, UIInteractionDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIScrollViewDelegate, UITextViewDelegate {

	//MARK: IBOutlets
	@IBOutlet weak var saveButton: UXButton!
	@IBOutlet weak var notesTextView: UITextView!
	@IBOutlet weak var newEventLabel: UILabel!
	@IBOutlet weak var eventTypeButton: UIButton!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var notesCard: UIView!
	@IBOutlet weak var sliderValueLabel: UILabel!
	@IBOutlet weak var slider: UISlider!
	@IBOutlet weak var unpaidSegment: UISegmentedControl!
	
	//MARK: IBActions
	@IBAction func eventTypeButtonTapped(_ sender: Any) {
		showPickerView()
	}
	
	@IBAction func optionsButtonTapped(_ sender: Any) {
		showOptionsMenu()
	}
	
	@IBAction func sliderDidChange(_ sender: Any) {
		let string = Double(slider.value).string(roundedTo: .noDecimals)
		sliderValueLabel.text = string
	}
	
	@IBAction func dataButtonTapped(_ sender: Any) {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "studyData") as! StudyDataVC
		vc.viewModel = WorkLogStore(timePeriod: .any)
		vc.studyProfile = StudyProfiles.workLog
		
		vc.navigationItem.title = "Work Log Data"
		navigationController?.pushViewController(vc, animated: true)
	}
	
	//MARK: Properties
	var defaultText = "Add notes here..."
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		makeBold(newEventLabel)
		saveButton.delegate = self
		
		setupPickerView()
		
		//Text View borders
		let insetAmount: CGFloat = 12
		notesCard.layer.cornerRadius = 10
		notesCard.addShadows(color: UIColor(named: "shadow") ?? .black, opacity: 0.1, radius: 3)
		notesTextView.layer.cornerRadius = 10
		notesTextView.textContainerInset = UIEdgeInsets(top: insetAmount, left: insetAmount, bottom: insetAmount, right: insetAmount)
		
		//keyboarding
		scrollView.delegate = self
		notesTextView.delegate = self
		keyboardManager.subcribeToKeyboardNotifications()
	}
	
	//MARK: UIPickerView Configuration
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		WorkLogType.allCases.count
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		WorkLogType.allCases[row].rawValue
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		let selection = WorkLogType.allCases[row].rawValue
		UIView.animate(withDuration: 0.15) {self.eventTypeButton.setTitle(selection, for: .normal) }
	}
	
	let picker = UIPickerView()
	let dummy = UITextField(frame: .zero)
	
	func showPickerView() {
		dummy.becomeFirstResponder()
	}
	
	func hidePickerView() {
		dummy.resignFirstResponder()
	}
	
	func setupPickerView() {
		picker.dataSource = self
		picker.delegate = self
		view.addSubview(dummy)
		dummy.inputView = picker
	}
	
	//MARK: Entry Saving
	private func saveEntry() {
		let context = CoreData.shared.viewContext
		let entry = WorkLogEntry(context: context)
		entry.eventType = WorkLogType.allCases[picker.selectedRow(inComponent: 0)].rawValue
		entry.dateRecorded = Date()
		if unpaidSegment.selectedSegmentIndex == 0 {
			entry.didWorkUnpaidHours = false
		} else {
			entry.didWorkUnpaidHours = true
		}
		entry.overworkedRating = Int16(slider.value)
		if notesTextView.text != defaultText {
			entry.notes = notesTextView.text
		}
		entry.saveContext()
	}
	
	//MARK: UIInteractionDelegate
	public func buttonTouchUp() {
		saveEntry()
		navigationController?.popViewController(animated: true)
	}
	
	//MARK: Form Management
	private lazy var keyboardManager: KeyboardManager = {
		let km = KeyboardManager(scrollView: self.scrollView)
		km.targetView = self.notesTextView
		return km
	}()
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView.isTracking { notesTextView.resignFirstResponder() }
	}
	
	func textViewDidBeginEditing(_ textView: UITextView) {
		if textView.text == defaultText { textView.text = String() }
	}
	
	private func resetForm() {
		eventTypeButton.setTitle("Unclassified", for: .normal)
		notesTextView.text = defaultText
		dummy.resignFirstResponder()
	}

}

extension NewWorkLogVC {
	public func showOptionsMenu() {
		
		let optOut = UIAlertAction(title: "Opt Out", style: .destructive) { (_) in
			StudyProfiles.workLog.optedIn.object = false
			self.navigationController?.popViewController(animated: true)
		}
		
		let ac = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
		ac.addAction(optOut)
		
		present(ac, animated: true, completion: nil)
	}
}
