import UIKit
import Combine

class StudyHostVC: UIPageViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var segmentedControl: UISegmentedControl!
	
	//MARK: IBActions
	@IBAction func segmentedControlValueChanged(_ sender: Any) {
		syncPagesToSegmentedState()
	}
	
	@IBAction func dataButtonTapped(_ sender: Any) {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "studyData") as! StudyDataVC
		vc.viewModel = studyProfile.dataStore
		vc.studyProfile = studyProfile
		vc.navigationItem.title = studyProfile.displayName
		navigationController?.pushViewController(vc, animated: true)
	}
	
	@IBAction func moreButtonTapped(_ sender: Any) {
		showOptionsMenu()
	}
	
	//MARK: Properties
	private var cancellables = [AnyCancellable]()
	public var studyProfile: StudyProfile!
	private let pageManager = PageManager()
	
	private lazy var pages: [UIViewController] = {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let filterable = studyProfile.filterable
		let vc = studyProfile.todayProvider(filterable)
		let vc2 = storyboard.instantiateViewController(identifier: "studyCalendar") as! StudyCalendarVC
		vc2.filterableStore = filterable
		return [vc, vc2]
	}()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		
		/// Configure page manager
		dataSource = pageManager
		delegate = pageManager
		pageManager.controller = self
		pageManager.pages = pages
		setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
		
		///Bind view model
		bind()
		
		view.backgroundColor = UIColor(named: "background")
	}
	
	public func bind() {
		studyProfile.filterable.publishedFilterDate
			.sink { _ in self.showTodayViewIfNecessary() }
		.store(in: &cancellables)
	}
	
	//MARK: Page Management
	func showTodayViewIfNecessary() {
		if segmentedControl.selectedSegmentIndex == 1 {
			pageManager.goBack(to: 0)
			segmentedControl.selectedSegmentIndex = 0
		}
	}
	
	private func syncPagesToSegmentedState() {
		switch segmentedControl.selectedSegmentIndex {
			case 0: pageManager.goBack(to: 0)
			case 1: pageManager.goForward(to: 1)
			default: debug("Unknown segment")
		}
	}

}

//MARK: Options Menu
extension StudyHostVC {
	public func showOptionsMenu() {
		
		let optOut = UIAlertAction(title: "Opt Out", style: .destructive) { (_) in
			self.studyProfile.optedIn.object = false
			self.navigationController?.popViewController(animated: true)
		}
		
		let ac = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
		ac.addAction(optOut)
		ac.addAction(UIAlertAction.cancelAction)
		
		present(ac, animated: true, completion: nil)
	}
}
