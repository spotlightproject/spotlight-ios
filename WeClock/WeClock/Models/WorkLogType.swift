import Foundation

enum WorkLogType: String, CaseIterable {
	case unclassified = "Unclassified"
	case verbalAbuse = "Verbal Abuse"
	case sexualHarassment = "Sexual Harassment"
	case racism = "Racism"
	case injury = "Injury"
	case mansplaining = "Mansplaining"
	case possibleCriminalActivity = "Possible Criminal Activity"
	case healthCodeViolation = "Health Code Violation"
	case terribleCoffee = "Terrible Coffee"
}
