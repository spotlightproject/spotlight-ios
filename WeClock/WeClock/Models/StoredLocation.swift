import CoreLocation
import CoreData

extension StoredLocation {

	//MARK: Determining Distance Between Locations
	// Wraps the CLLocation distance(from:) method and makes it accessible to StoredLocation objects.
	// The distance is measured in meters.
	public func distance(from otherLocation: StoredLocation) -> Double {
		let locationOne = flatCLLocation
		let locationTwo = otherLocation.flatCLLocation
		let distance = locationOne.distance(from: locationTwo)
		return distance
	}
	
	//MARK: Saving CLLocations to Core Data
	public static func save(_ location: CLLocation, in context: NSManagedObjectContext, service: String, accuracy: Double) {
		//Geocode Location and save on completion
		/*
		CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
			if let error = error { debug("Error geocoding location", save: true, data: error)}
			
			guard let placemark = placemarks?.first else {
				debug("Unable to geocode any placemarks from CLLocation.", save: true); return
			}
			
			context.perform {
				debug("Saving stored location in background")
				storedLocation.countryName = placemark.country
				storedLocation.stateAbbreviation = placemark.administrativeArea
				storedLocation.cityName = placemark.locality
				storedLocation.placeName = placemark.name
				storedLocation.streetName = placemark.thoroughfare
				storedLocation.latitude = location.coordinate.latitude
				storedLocation.longitude = location.coordinate.longitude
				storedLocation.dateVisited = Date()

				storedLocation.saveContext()
			}
			
		}*/
		context.perform {
			let storedLocation: StoredLocation = StoredLocation(context: context)
			
			//Add source and accuracy metadata
			storedLocation.horizontalAccuracy = accuracy
			storedLocation.clServiceSource = service
			
			//add location
			storedLocation.latitude = location.coordinate.latitude
			storedLocation.longitude = location.coordinate.longitude
			storedLocation.dateVisited = Date()
			storedLocation.saveContext()
		}

	}
	
	
	//MARK: Computed Properties
	/*
		Returns a CLLocation object without any altitude, date, accuracy, course or speed data.
		This is mainly for calculating distance between locations, to be used with CLLocation:distance(from:) */
	public var flatCLLocation: CLLocation {
		CLLocation(latitude: self.latitude, longitude: self.longitude)
	}
	
	public var accuracyDescription: String {
		let accuracy = self.horizontalAccuracy
		if accuracy <= 0 {
			return "Unknown"
		} else {
			return accuracy.string(roundedTo: .twoPlaces)
		}
	}
	
	public var isSufficientlyAccurate: Bool {
		horizontalAccuracy < 125
	}
	
	var coordinates: CLLocationCoordinate2D {
		CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
	}
	
	//MARK: Placemark Details
	func placemark(_ completion: @escaping (CLPlacemark) -> Void) {
		CLGeocoder().reverseGeocodeLocation(self.flatCLLocation) { (placemarks, errorOrNil) in
			if let error = errorOrNil { debug("Error rev-geocoding placemark.", data: error)}
			
			if let placemark = placemarks?.first {
				completion(placemark)
			}
		}
	}
	
	func updatePlacemarkDetails(_ done: (() -> Void)? = nil) {
		self.placemark { (placemark) in
			self.countryName = placemark.country
			self.stateAbbreviation = placemark.administrativeArea
			self.cityName = placemark.locality
			self.placeName = placemark.name
			self.streetName = placemark.thoroughfare
			done?()
		}
	}
	
	func addPlacemarkDetailsIfMissing(_ done: (() -> Void)? = nil) {
		if self.placeName != nil {
			done?(); return
		}
		self.placemark { (placemark) in
			self.countryName = placemark.country
			self.stateAbbreviation = placemark.administrativeArea
			self.cityName = placemark.locality
			self.placeName = placemark.name
			self.streetName = placemark.thoroughfare
			done?()
		}
	}
}


extension Array where Element == StoredLocation {
	
	//MARK: Distance Calculations
	public var crudeDistance: Double {
		var previousLocation: StoredLocation?
		
		let distance = self.reduce(into: Double(0)) { (sum, current) in
			if let previous = previousLocation {
				sum += previous.distance(from: current)
			}
			previousLocation = current
		}
		return distance / Units.Conversions.MetersPerMile
	}
}
