import Foundation
import UIKit

enum StudyProfiles {
	
	static var all: [StudyProfile] {
		[dailyMovement, locationsAndDistance, workLog, commuteTime, environmentalNoise, batteryUsage]
	}
	
	static var phoneOnly: [StudyProfile] {
		[dailyMovement, locationsAndDistance, workLog, batteryUsage]
	}
	
	static var available: [StudyProfile] {
		if Settings.shared.hasPairedAppleWatch.object {
			return all
		} else {
			return phoneOnly
		}
	}
	
	static var dailyMovement: StudyProfile {
		StudyProfile(
		displayName: "Daily Movement",
		iconName: "bolt.circle",
		tintColor: .systemYellow,
		optedIn: Settings.shared.movementOptedIn,
		summaryStore: MovementViewModel(timePeriod: .today),
		dataStore: MoveSamplesStore(timePeriod: .any),
		filterable: MovementViewModel(timePeriod: .today),
		todayProvider: { filterable in
			let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
			let vc = storyboard.instantiateViewController(withIdentifier: "dailyMovementToday") as! DailyMovementTodayVC
			vc.viewModel = filterable
			return vc
		}
		)
	}

	static var locationsAndDistance: StudyProfile {
		StudyProfile(
			displayName: "Locations and Distance",
			iconName: "mappin.circle",
			tintColor: .systemTeal,
			optedIn: Settings.shared.locationsOptedIn,
			summaryStore: LocationsStore(timePeriod: .today),
			dataStore: LocationsStore(),
			filterable: LocationsStore(timePeriod: .today),
			todayProvider: { filterable in
				let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
				let vc = storyboard.instantiateViewController(withIdentifier: "locationsAndDistanceToday") as! LocationsTodayVC
				vc.viewModel = filterable
				return vc
			}
		)
	}

	static var workLog: StudyProfile {
		StudyProfile(
			displayName: "Work Log",
			iconName: "doc.circle",
			tintColor: .systemPurple,
			optedIn: Settings.shared.workLogOptedIn,
			summaryStore: WorkLogStore(timePeriod: .today),
			dataStore: WorkLogStore(timePeriod: .any),
			filterable: WorkLogStore(timePeriod: .today),
			todayProvider: { _ in UIViewController() }
		)
	}

	static var commuteTime: StudyProfile {
		StudyProfile(
			displayName: "Commute Time",
			iconName: "car",
			tintColor: .systemBlue,
			optedIn: Settings.shared.commuteOptedIn,
			summaryStore: CommuteStore(timePeriod: .today),
			dataStore: CommuteStore(timePeriod: .any),
			filterable: CommuteStore(timePeriod: .today),
			todayProvider: {  filterable in
				let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
				let vc = storyboard.instantiateViewController(withIdentifier: "commuteToday") as! CommuteTimeTodayVC
				vc.viewModel = filterable
				return vc
			}
		)
	}

	static var environmentalNoise: StudyProfile {
		StudyProfile(
			displayName: "Environmental Noise",
			iconName: "ear",
			tintColor: .systemPink,
			optedIn: Settings.shared.environmentalNoiseOptedIn,
			summaryStore: EnvironmentalNoiseViewModel(timePeriod: .today),
			dataStore: NoiseSamplesStore(timePeriod: .any),
			filterable: EnvironmentalNoiseViewModel(timePeriod: .today),
			todayProvider: { filterable in
				let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
				let vc = storyboard.instantiateViewController(withIdentifier: "environmentalNoiseToday") as! EnvironmentalNoiseTodayVC
				vc.viewModel = filterable
				return vc
			}
		)
	}
	
	static var batteryUsage: StudyProfile {
		StudyProfile(
			displayName: "Battery Usage",
			iconName: "battery.25",
			tintColor: .systemOrange,
			optedIn: Settings.shared.batteryLevelsOptedIn,
			summaryStore: BatteryStore(timePeriod: .today),
			dataStore: BatteryStore(),
			filterable: BatteryStore(timePeriod: .today),
			todayProvider: { filterable in
				let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
				let vc = storyboard.instantiateViewController(withIdentifier: "batteryReadingsToday") as! BatteryReadingsTodayVC
				vc.viewModel = filterable
				return vc
			}
		)
	}
}
