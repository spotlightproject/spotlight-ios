import Foundation
import UIKit

struct StudyProfile: Hashable {
	
	//MARK: Properties
	var displayName: String
	var iconName: String
	var tintColor: UIColor
	var optedIn: SharedSetting<Bool>
	var summaryStore: SummaryProvider
	var dataStore: StudyStore
	var filterable: Filterable
	var todayProvider: (Filterable) -> UIViewController
	
	//MARK: Equatable
	static func == (lhs: StudyProfile, rhs: StudyProfile) -> Bool {
		lhs.displayName == rhs.displayName
	}
	
	//MARK: Hashable
	func hash(into hasher: inout Hasher) {
        hasher.combine(displayName)
    }
}


extension Array where Element == StudyProfile {
	var optedIn: [StudyProfile] {
		reduce(into: []) { (results, profile) in
			if profile.optedIn.object == true { results.append(profile) }
		}
	}
}
