import Foundation
import UIKit
import HealthKit

/// Describes any model which is convertible into a `StudyResult`. Most data types in the app will conform to this. `HKWorkout`, `HKQuantitySample`, `StoredLocation`, and `WorkLogEntry` all conform.
public protocol StudyResultConvertible {
	var studyResultName: String { get }
	var studyResultDescription: String { get }
	var studyResultDate: Date? { get }
	var studyResultContent: String { get }
	var detailController: UIViewController { get }
	var studyResultUniqueID: Int { get }
}

/// A convenience method for convert building a study result from a convertible.
extension StudyResultConvertible {
	var studyResult: StudyResult {
		StudyResult(
			seed: self.studyResultUniqueID,
			name: self.studyResultName,
			date: self.studyResultDate,
			content: self.studyResultContent
		)
	}
}

//MARK: Conformance
extension StoredLocation: StudyResultConvertible {
	
	public var studyResultName: String {
		"geologging"
	}
	
	public var studyResultDescription: String {
		self.placeName ?? "\(self.latitude) / \(self.longitude)"
	}
	
	public var studyResultDate: Date? {
		dateVisited
	}
	
	public var studyResultContent: String {
		self.placeName ?? "\(coordinates.latitude), \(coordinates.longitude)"
	}

	public var detailController: UIViewController {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "locationDetails") as! LocationDetailVC
		vc.location = self
		return vc
	}
	
	public var studyResultUniqueID: Int {
		Int(coordinates.latitude) + Int(coordinates.longitude) + (dateVisited?.hashValue ?? 1)
	}
}

extension HKQuantitySample: StudyResultConvertible {
	public var studyResultName: String {
		friendlyTitle
	}
	
	public var studyResultDescription: String {
		friendlyDescription
	}
	
	public var studyResultDate: Date? {
		startDate
	}
	
	public var studyResultContent: String {
		friendlyDescription
	}
	
	public var detailController: UIViewController {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "movementDataDetail") as! HKQuantitySampleDetailsVC
		vc.sample = self
		return vc
	}
	
	public var studyResultUniqueID: Int {
		hashValue
	}
}

extension HKWorkout: StudyResultConvertible {
	public var studyResultName: String {
		"commute"
	}
	
	public var studyResultDescription: String {
		let minutes = self.duration / 60
		return "\(minutes.string(roundedTo: .onePlace)) minutes"
	}
	
	public var studyResultDate: Date? {
		startDate
	}
	
	public var studyResultContent: String {
		"\((duration / 60).string(roundedTo: .twoPlaces)) minutes"
	}
	
	public var detailController: UIViewController {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "commuteDetail") as! CommuteDetailVC
		vc.workout = self
		return vc
	}
	
	public var studyResultUniqueID: Int {
		hashValue
	}
}

extension WorkLogEntry: StudyResultConvertible {
	public var studyResultName: String {
		"work log"
	}
	
	public var studyResultDescription: String {
		self.eventType ?? "Unclassified"
	}
	
	public var studyResultDate: Date? {
		dateRecorded
	}
	
	public var studyResultContent: String {
		eventType ?? "Unclassified"
	}
	
	public var detailController: UIViewController {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "workLogDetails") as! WorkLogDetailsVC
		vc.entry = self
		return vc
	}
	
	public var studyResultUniqueID: Int {
		hashValue
	}
}

extension BatteryReading: StudyResultConvertible {
	public var studyResultName: String {
		"battery reading"
	}
	
	public var studyResultDescription: String {
		/// empty for now
		String()
	}
	
	public var studyResultDate: Date? {
		date
	}
	
	public var studyResultContent: String {
		let percentage = Double(level * 100)
		let rounded = percentage.string(roundedTo: .onePlace)
		return "\(rounded)%"
	}
	
	public var detailController: UIViewController {
		let storyboard = UIStoryboard(name: "Study", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "batteryReadingDetails") as! BatteryReadingDetailsVC
		vc.reading = self
		return vc
	}
	
	public var studyResultUniqueID: Int {
		hashValue
	}
}
