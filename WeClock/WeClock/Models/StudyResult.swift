import Foundation
import UIKit

public struct StudyResult: Hashable {
	
	/// Need a unique seed to prevent collisions when hashing the object for a diffable data source
	var seed: Int
	
	let name: String
	let date: Date?
	let content: String
	
	var csvRow: [String] {
		[String(hashValue), name, DateManager.shared.shortString(for: date), content]
	}
}

extension Array where Element == StudyResult {
	public func export(from vc: UIViewController) {
		
		/// Build csv
		let csv = CSV()
		for result in self {
			csv.add(row: result.csvRow)
		}
		
		/// Store to disk
		csv.save()
		
		/// Share temp file URL
		let sc = UIActivityViewController(
			activityItems: [csv.temporaryURL],
			applicationActivities: nil
		)
		vc.present(sc, animated: true, completion: nil)
	
	}
}
