import Foundation

/*
	A namespace for units and measurement constants.
*/
enum Units {
	
	enum Conversions {
		public static let MetersPerMile: Double = 1609.34
	}
}
