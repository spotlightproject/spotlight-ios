import Foundation

/*
	A namespace for keys and identifiers
*/
enum Keys {
	
	public static let CustomWorkoutType: String = "CustomWorkoutTypeKey"
}

enum Identifiers {
	public static let CommuteWorkout: String = "CommuteWorkoutType"
}
