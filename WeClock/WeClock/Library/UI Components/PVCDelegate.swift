import UIKit

class PVCDelegate: NSObject, UIPageViewControllerDelegate {
	
	var pages: [UIViewController] = []
	
	//MARK: UIPageViewController Delegate
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let currentIndex = self.nextIndex {
            transitionDidComplete(to: currentIndex)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let nextViewController = pendingViewControllers.first, let nextIndex = pages.firstIndex(of: nextViewController) {
            self.nextIndex = nextIndex
        }
    }
	
	//MARK: Imperative Control
	var nextIndex: Int?
    var currentIndex: Int = 0
	
    
    func transitionDidComplete(to index: Int) {
//       pageControl?.currentPage = index
        self.nextIndex = nil
        self.currentIndex = index
    }
}
