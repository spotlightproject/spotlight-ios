import UIKit

/// This adds swipe-to-delete support for `UIDiffableDataSource`
class UXDiffableDataSource<S: Hashable, T: Hashable>: UITableViewDiffableDataSource<S, T> {
    // ...
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    // ...
}
