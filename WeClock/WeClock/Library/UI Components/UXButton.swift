import UIKit

@IBDesignable
class UXButton: UIButton {
	
	//MARK: Properties
	var delegate: UIInteractionDelegate?
	
	//MARK: Interface Builder
    @IBInspectable
    var cornerRadius: CGFloat = 2.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
	
	@IBInspectable
	var disableOnTap: Bool = false
	
	@IBInspectable
	var disabledColor: UIColor = .lightGray
	
	@IBInspectable
	var highlightedColor: UIColor = .red
	
	@IBInspectable
	var defaultColor: UIColor = .blue
	
	//MARK: Initializers
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}
	
	func setup() {
		let tap = UILongPressGestureRecognizer(target: self, action: #selector(tapHandler))
        tap.minimumPressDuration = 0
        self.addGestureRecognizer(tap)
	}
	
	//MARK: Touch Handling
    @objc func tapHandler(gesture: UITapGestureRecognizer) {
        if gesture.state == .began {
			animateHighlightedColor()
			animateShrink()
        } else if gesture.state == .ended {
            disableIfNecessary()
			animateExpand()
			delegate?.buttonTouchUp()
        }
    }
	
	//MARK: State Management
	private func disableIfNecessary() {
		if disableOnTap {
			self.isEnabled = false
			animateDisabledColor()
		} else {
			animateDefaultColor()
		}
	}
	
	//MARK: Animations
	private func animateDefaultColor() {
		UIView.animate(withDuration: 0.5) {
			self.backgroundColor = self.defaultColor
		}
	}
	
	private func animateHighlightedColor() {
		UIView.animate(withDuration: 0.15) {
			self.backgroundColor = self.highlightedColor
		}
	}
	
	private func animateDisabledColor() {
		UIView.animate(withDuration: 0.15) {
			self.backgroundColor = self.disabledColor
		}
	}
	
	private func animateShrink() {
		let pulseAnimation = CABasicAnimation(keyPath: "transform.scale")
		pulseAnimation.duration = 0.15
		pulseAnimation.fromValue = 1
		pulseAnimation.toValue = 0.95
		//Autoreverse
		pulseAnimation.autoreverses = false
		pulseAnimation.repeatCount = 0
		//Timing
		pulseAnimation.timingFunction = CAMediaTimingFunction(name: .easeOut)
		
		//Fill mode
		pulseAnimation.fillMode = .forwards
		pulseAnimation.isRemovedOnCompletion = false
		
		self.layer.add(pulseAnimation, forKey: "pressDown")
	}
	
	private func animateExpand() {
		let pulseAnimation = CASpringAnimation(keyPath: "transform.scale")
		pulseAnimation.duration = 0.2
		pulseAnimation.fromValue = 0.95
		pulseAnimation.toValue = 1
		//Autoreverse
		pulseAnimation.autoreverses = false
		pulseAnimation.repeatCount = 0
		//Timing
		
		pulseAnimation.damping = 5
		pulseAnimation.initialVelocity = 1
		
		//Fill mode
		pulseAnimation.fillMode = .forwards
		pulseAnimation.isRemovedOnCompletion = false
		
		self.layer.add(pulseAnimation, forKey: "pressUp")
	}
}

protocol UIInteractionDelegate {
	func buttonTouchUp()
}
