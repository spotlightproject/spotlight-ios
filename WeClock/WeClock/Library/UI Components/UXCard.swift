import UIKit

@IBDesignable
class UXView: UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}

extension UIView {
	
	public func addShadows(color: UIColor, opacity: Float, radius: CGFloat) {
		self.layer.shadowColor = color.cgColor
		self.layer.shadowOpacity = opacity
		self.layer.shadowOffset = CGSize(width: 1, height: 3)
		self.layer.shadowRadius = radius
	}
}
