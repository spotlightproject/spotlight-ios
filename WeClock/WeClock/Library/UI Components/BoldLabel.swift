import UIKit

class BoldLabel: UILabel {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}
	
	func setup() {
		let size = self.font.fontDescriptor.pointSize
		let font = UIFont.systemFont(ofSize: size, weight: .bold)
		self.font = font
	}
}

extension UILabel {
	func embolden() {
		let size = font.fontDescriptor.pointSize
        let emboldenedFont = UIFont.systemFont(ofSize: size, weight: .bold)
		self.font = emboldenedFont
	}
}
