import Foundation
import UIKit

class BlurredCard: UIView {
	
	//MARK: Properties
	public var cornerRadius: CGFloat = 0 {
		didSet {
			containerView.layer.cornerRadius = cornerRadius
			effectView.layer.cornerRadius = cornerRadius
		}
	}
	
	public var maskedCorners: CACornerMask = [] {
		didSet {
			containerView.layer.maskedCorners = maskedCorners
		}
	}
	
	//MARK: Subview
	let containerView = UIView()
	
	private lazy var effectView: UIVisualEffectView = {
		let blur = UIBlurEffect(style: .prominent)
		let view = UIVisualEffectView(effect: blur)
		return view
	}()
	
	//MARK: Initialization
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}
	
	private func setup() {
		insertSubview(containerView, at: 0)
		containerView.insertSubview(effectView, at: 0)
		
		/// Default values
//		clipsToBounds = true
		containerView.clipsToBounds = true
		maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
		cornerRadius = 25
	}
	
	//MARK: Layout
	override func layoutSubviews() {
		super.layoutSubviews()
		containerView.frame = bounds
		effectView.frame = bounds
	}
}
