import UIKit

class PageManager: NSObject, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
	
	public var controller: UIPageViewController!
	
	public lazy var pages: [UIViewController] = []
	
	//MARK: Methods
	public func set(pageNames: [String], storyboard: String) {
		let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
		var pages: [UIViewController] = []
		for name in pageNames {
			let vc = storyboard.instantiateViewController(identifier: name)
			pages.append(vc)
		}
		controller.setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
	}
	
	//MARK: PageViewController Data Source
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of: viewController)!
        let previousIndex = currentIndex - 1
        if previousIndex < 0 { return nil } //stop when index is 0
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of: viewController)!
        let nextIndex = currentIndex + 1
        if nextIndex >= pages.count { return nil } //stop when index is maxed
        return pages[nextIndex]
    }
	
	//MARK: UIPageViewController Delegate
		func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
			if let currentIndex = self.nextIndex {
				transitionDidComplete(to: currentIndex)
			}
		}
		
		func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
			if let nextViewController = pendingViewControllers.first, let nextIndex = pages.firstIndex(of: nextViewController) {
				self.nextIndex = nextIndex
			}
		}
		
		//MARK: Imperative Control
		var nextIndex: Int?
		var currentIndex: Int = 0
		
		func goForward(to pageIndex: Int) {
			let pendingPages = [pages[pageIndex]]
			controller.setViewControllers(pendingPages, direction: .forward, animated: true) { (success) in
				if success { self.transitionDidComplete(to: pageIndex) }
			}
		}
		
		func goBack(to pageIndex: Int) {
			let pendingPages = [pages[pageIndex]]
			controller.setViewControllers(pendingPages, direction: .reverse, animated: true) { (success) in
				if success { self.transitionDidComplete(to: pageIndex) }
			}
		}
		
		func transitionDidComplete(to index: Int) {
	//        pageControl?.currentPage = index
			self.nextIndex = nil
			self.currentIndex = index
		}
}

protocol UIPageControllerPage {
	var pageManager: PageManager! { get set }
}
