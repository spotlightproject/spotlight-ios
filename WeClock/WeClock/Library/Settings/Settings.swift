import Foundation
import CoreLocation

///A global wrapper for user preferences.
///These properties map and unmap preferences to primitive objects saved in UserDefaults.
public class Settings: NSObject {
	
	//MARK: Shared Instance
	static var shared: Settings = Settings()
	
    @Shared(key: "hasLaunchedPreviously", defaultValue: false)
    var hasLaunchedPreviously
    
    @Shared(key: "hasAskedForHKPermission", defaultValue: false)
	var hasAskedForHKPermission
	
	//MARK: Preferred Location Service
	var preferredLocationService: LocationServices.ServiceType? {
		get {
			let integer = UserDefaults.standard.integer(forKey: "preferredLocationService")
			return LocationServices.ServiceType(rawValue: integer)
		}
		set {
			if let serviceType = newValue {
				let integer = serviceType.rawValue
				UserDefaults.standard.set(integer, forKey: "preferredLocationService")
			}
		}
	}
	
	//MARK: Work and Home Locations
	var workLocation: CLLocation {
		get {
			let latitude = UserDefaults.standard.double(forKey: "workLatitude")
			let longitude = UserDefaults.standard.double(forKey: "workLongitude")
			return CLLocation(latitude: latitude, longitude: longitude)
		}
		set {
			let latitude = newValue.coordinate.latitude
			let longitude = newValue.coordinate.longitude
			UserDefaults.standard.set(latitude, forKey: "workLatitude")
			UserDefaults.standard.set(longitude, forKey: "workLongitude")
		}
	}
	
	var homeLocation: CLLocation {
		get {
			let latitude = UserDefaults.standard.double(forKey: "homeLatitude")
			let longitude = UserDefaults.standard.double(forKey: "homeLongitude")
			return CLLocation(latitude: latitude, longitude: longitude)
		}
		set {
			let latitude = newValue.coordinate.latitude
			let longitude = newValue.coordinate.longitude
			UserDefaults.standard.set(latitude, forKey: "homeLatitude")
			UserDefaults.standard.set(longitude, forKey: "homeLongitude")
		}
	}
	
	//MARK: Workdays of the Week
	var workdaysOfWeek: [String: Bool]? {
		get {
			return UserDefaults.standard.dictionary(forKey: "workdaysOfWeek") as? [String: Bool]
		}
		set {
			UserDefaults.standard.set(newValue, forKey: "workdaysOfWeek")
		}
	}
	
	//MARK: Workday Start/End Times
	var workdayStartTime: Date? {
		get {
            let interval = UserDefaults.standard.double(forKey: "workdayStartTime")
			if interval == 0 {
				return nil
			} else {
				let storedDate = Date(timeIntervalSince1970: interval)
				return adjustedDateForToday(storedDate)
			}
        }
        set {
			if let interval = newValue?.timeIntervalSince1970 {
				UserDefaults.standard.set(interval, forKey: "workdayStartTime")
			}
        }
	}
	
	var workdayEndTime: Date? {
		get {
            let interval = UserDefaults.standard.double(forKey: "workdayEndTime")
			if interval == 0 {
				return nil
			} else {
				let storedDate = Date(timeIntervalSince1970: interval)
				return adjustedDateForToday(storedDate)
			}
        }
        set {
			if let interval = newValue?.timeIntervalSince1970 {
				UserDefaults.standard.set(interval, forKey: "workdayEndTime")
			}
        }
	}
	
	private func adjustedDateForToday(_ date: Date?) -> Date? {
		guard let date = date else { return nil }
		let startComponents = Calendar.current.dateComponents([.hour, .minute], from: date)
		let todayStart = Calendar.current.startOfDay(for: Date())
		let startTime = Calendar.current.date(byAdding: startComponents, to: todayStart)
		return startTime
	}
	
	//MARK: Version Number
	@Shared(key: "lastRecordedVersion", defaultValue: 0.0)
	var lastRecordedVersion
	
	//MARK: Studies Opted In
	private enum Keys {
		static var CommuteOptedIn: String = "commuteOptedIn"
		static var MovemenentOptedIn: String = "movementOptedIn"
		static var LocationsOptedIn: String = "locationsOptedIn"
		static var WorkLogOptedInt: String = "workLogOptedIn"
	}

	@Shared(key: Keys.MovemenentOptedIn, defaultValue: false)
	var movementOptedIn
	
	@Shared(key: Keys.LocationsOptedIn, defaultValue: false)
	var locationsOptedIn
	
	@Shared(key: Keys.WorkLogOptedInt, defaultValue: false)
	var workLogOptedIn
	
	@Shared(key: Keys.CommuteOptedIn, defaultValue: false)
	var commuteOptedIn
	
	@Shared(key: "environmentalNoiseOptedIn", defaultValue: false)
	var environmentalNoiseOptedIn
	
	@Shared(key: "batteryLevelsOptedIn", defaultValue: false)
	var batteryLevelsOptedIn
	
	@Shared(key: "allHealthDataOptedIn", defaultValue: false, sideEffect: {
		Settings.shared.environmentalNoiseOptedIn.object = $0
		Settings.shared.commuteOptedIn.object = $0
		Settings.shared.movementOptedIn.object = $0
	})
	var allHealthDataOptedIn
	
	@Shared(key: "hasPairedAppleWatch", defaultValue: false)
	var hasPairedAppleWatch
	
}


