import Foundation
import SwiftUI

class CodableSetting<O: Codable>: ObservableObject {
	
	//MARK: Initializer
	internal init(key: String, defaultValue: O, sideEffect: ((O) -> Void)? = nil) {
		self.key = key
		self.defaultValue = defaultValue
		self.sideEffect = sideEffect
		
		/// Decode
		if let data = UserDefaults.standard.data(forKey: key) {
			let decoded = try? JSONDecoder().decode(O.self, from: data)
			self.object = decoded ?? defaultValue
		} else {
			self.object = defaultValue
		}
	}
	
	//MARK: Object
	private var key: String
	private var defaultValue: O
	
	//MARK: Observables
	@Published public var object: O {
		didSet { self.persist(); sideEffect?(object) }
	}
	
	//MARK: Persistence
	private func persist() {
		if let data = try? JSONEncoder().encode(object) {
			UserDefaults.standard.set(data, forKey: key)
		}
	}
	
	//MARK: Binding Provider
	public lazy var binding: Binding<O> = {
		Binding(get: {
			return self.object
		}) { (newValue) in
			self.object = newValue
		}
	}()
	
	//MARK: Side Effects
	private var sideEffect: ((O) -> Void)?
}

@propertyWrapper
struct Encoded<T: Codable> {
	
	//MARK: Properties
	var shared: CodableSetting<T>
	
	//MARK: Initializer
	internal init(key: String, defaultValue: T, sideEffect: ((T) -> Void)? = nil) {
		shared = CodableSetting(key: key, defaultValue: defaultValue, sideEffect: sideEffect)
	}
	
	//MARK: Property Wrapper
	public var wrappedValue: CodableSetting<T> {
		get { shared }
    }
}
