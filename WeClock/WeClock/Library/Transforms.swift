import Foundation

/*
	Transforms for common maps.
*/
enum Transforms {
	
	static var DoubleToRoundedString: (Double) -> String = {
		String(Int($0))
	}
}
