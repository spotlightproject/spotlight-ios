import Foundation
import UIKit

class KeyboardManager {
	
	//MARK: Properties
	let scrollView: UIScrollView
	
	//MARK: Initialization
	internal init(scrollView: UIScrollView) {
		self.scrollView = scrollView
	}
	
	var targetView: UIView?
	var selectedView: UIView?
	
    func subcribeToKeyboardNotifications() {
        let nc = NotificationCenter.default
        nc.addObserver(forName: UIResponder.keyboardDidShowNotification, object: nil, queue: nil, using: keyboardShown(_:))
        nc.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: nil, using: keyboardHidden(_:))
    }
    
    func keyboardShown(_ notification: Notification) {
        guard let userInfo = notification.userInfo else {
            print("Couldn't get user info from notification.")
            return
        }
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            print("Couldn't get keyboard size from userInfo.")
            return
        }
        let keyboardFrame = keyboardSize.cgRectValue
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height + 35, right: 0)
		UIView.animate(withDuration: 0.5) {
			self.scrollView.contentInset = insets
		}
		
		self.scrollView.scrollRectToVisible(targetView!.frame, animated: true)
    }
    
    func keyboardHidden(_ notification: Notification) {
        UIView.animate(withDuration: 0.5) {
            self.scrollView.contentInset = .zero
        }
    }
}
