import CoreLocation
import MapKit

class RouteManager {
	
	//MARK: Properties
	public var delegate: RouteDrawingDelegate?
	public var transportType: MKDirectionsTransportType = .automobile
	
	private var locationStack: [StoredLocation] = []
	private var discoveredRoutes: [MKRoute] = []
	private var discoveredCoordinates: [CLLocationCoordinate2D] = []
	private var endCoordinates: [CLLocationCoordinate2D] = []
	private var recordedDistances: [CLLocationDistance] = []
	
	public var minimumDistanceBetweenLocations: Double = 50
	public var minimumHorizontalAccuracy: Double = 110
	
	public func start(with locations: [StoredLocation]) {
		reset()
		self.locationStack = locations
		next()
	}
	
	private func reset() {
		discoveredRoutes = []
		discoveredCoordinates = []
		recordedDistances = []
		currentLocation = nil
		queuedLocation = nil
		count = 1
	}
	
	public func done() {
		debug("Done processing routes.")
		debug("routes found - \(discoveredRoutes.count): \(discoveredRoutes)")
		delegate?.draw(routes: discoveredRoutes, coordinates: discoveredCoordinates, endCoordinates: endCoordinates)
	}

	var currentLocation: StoredLocation?
	var queuedLocation: StoredLocation?
	var count = 1
	
	private func next() {
		debug("Getting directions \(count)")
		
		if locationStack.count < 2 { done() }
		
		currentLocation = locationStack.popLast()
		queuedLocation = locationStack.last
		
		getRoute(from: currentLocation, to: queuedLocation)
		count += 1
	}
	
	private func retry(_ location: StoredLocation) {
		locationStack.append(location)
		next()
	}
	
	private func getRoute(from startLocation: StoredLocation?, to endLocation: StoredLocation?) {
		guard let startLocation = startLocation, let endLocation = endLocation else {
			debug("No more locations available")
			return
		}
		
		if !startLocation.isSufficientlyAccurate {
			debug("Skipping inaccurate START location")
			next()
			return
		}
		
		if !endLocation.isSufficientlyAccurate {
			debug("Skipping inaccurate END location")
			locationStack.removeLast()
			retry(startLocation)
			return
		}
		
		if startLocation.flatCLLocation.distance(from: endLocation.flatCLLocation) < minimumDistanceBetweenLocations {
			debug("Skipping next location as too close to current")
			debug("Retrying with next next location")
			locationStack.removeLast()
			retry(startLocation)
			return
		}
		
		//Create start/end locations
		let sourcePlacemark = MKPlacemark(coordinate: startLocation.coordinates, addressDictionary: nil)
		let sourceMapItem = MKMapItem(placemark: sourcePlacemark)

		let destPlacemark = MKPlacemark(coordinate: endLocation.coordinates)
		let destMapItem = MKMapItem(placemark: destPlacemark)
		
		//Add locations to request
		let request: MKDirections.Request = MKDirections.Request()
		request.source = sourceMapItem
		request.destination = destMapItem

		//request.requestsAlternateRoutes = true
		
		request.transportType = transportType
		
		// 4
		let directions = MKDirections(request: request)
		
		directions.calculate { (response, error) in
			if let error = error { debug("Error", data: error) }
			
			if let route = response?.routes.first {
				debug("Route found. Moving to next segment.")
				self.recordedDistances.append(route.distance)
				self.discoveredRoutes.append(route)
				self.discoveredCoordinates.append(startLocation.coordinates)
				self.endCoordinates.append(endLocation.coordinates)
				self.next()
			} else {
				debug("Couldn't find route. Retrying with next in queue.")
				self.locationStack.removeLast()
				self.retry(startLocation)
			}
		}
	}
	
	var accumulatedMiles: Double {
		var totalDistance: Double = 0
		for distance in recordedDistances {
			totalDistance += distance
		}
		let totalMiles = totalDistance / Units.Conversions.MetersPerMile
		return totalMiles
	}
}

protocol RouteDrawingDelegate {
	func draw(routes: [MKRoute], coordinates: [CLLocationCoordinate2D], endCoordinates: [CLLocationCoordinate2D])
}
