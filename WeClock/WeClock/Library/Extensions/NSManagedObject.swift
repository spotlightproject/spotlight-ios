import CoreData

extension NSManagedObject {
	
	public func saveContext() {
		debug("Saving context...")
		do {
			try self.managedObjectContext?.save()
			debug("Saved!")
		} catch {
			debug("Core Data save error", save: true, data: error)
		}
	}
}
