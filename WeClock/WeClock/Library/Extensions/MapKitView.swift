import MapKit

extension MKMapView {
	
	func center(at coordinate: CLLocationCoordinate2D, radius: Double, animated: Bool = false) {
		let coordinateRegion = MKCoordinateRegion(
			center: coordinate,
			latitudinalMeters: radius,
			longitudinalMeters: radius
		)
		self.setRegion(coordinateRegion, animated: animated)
	}
	
	func dropPin(at location: CLLocation, title: String?) {
		let annotation = MKPointAnnotation()
		annotation.coordinate = location.coordinate
		
		annotation.title = title
		self.addAnnotation(annotation)
	}
}
