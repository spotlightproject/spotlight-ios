import Foundation

extension Double {
    
    public func rounded(to roundingGuide: RoundingGuide) -> Double {
        let numberOfPlaces = roundingGuide.rawValue
        let factor = pow(Double(10), Double(numberOfPlaces))
        var x = self * factor
        x.round()
        let result = x / factor
        return result
    }
    
    public func string(roundedTo roundingGuide: RoundingGuide, showPlusSign: Bool = false) -> String {
        let roundedDouble = self.rounded(to: roundingGuide)
        var optionalPlusSign = String()
        if showPlusSign && roundedDouble > 0 {
            optionalPlusSign = "+"
        }
        if roundingGuide.rawValue == 0 {
            return optionalPlusSign + String(Int(roundedDouble))
        } else {
            return optionalPlusSign + String(roundedDouble)
        }
    }
}

public enum RoundingGuide: Int {
    case noDecimals = 0
    case onePlace = 1
    case twoPlaces = 2
    case threePlaces = 3
}
