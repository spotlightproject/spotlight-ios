import UIKit

extension UIViewController {
    func makeBold(_ label: UILabel) {
        let size = label.font.fontDescriptor.pointSize
        let font = UIFont.systemFont(ofSize: size, weight: .bold)
        label.font = font
    }
	
	func makeBold(_ labels: [UILabel]) {
		labels.forEach { makeBold($0) }
	}
}
