import Foundation

extension Date {
    
	internal static func startDate(of timePeriod: HKModel.TimePeriod) -> Date? {
        let calendar = Calendar.current
        
        switch timePeriod {
			case .today:
				let todayComponents = calendar.dateComponents([.day, .month, .year], from: Date())
				return calendar.date(from: todayComponents)
			case .thisWeek:
				let weekComponents = calendar.dateComponents([.weekOfYear, .yearForWeekOfYear], from: Date())
				if let calendarWeekStart = calendar.date(from: weekComponents) {
					return calendarWeekStart
				} else {
					debug("Couldn't construct date from HKTimePeriod: \(timePeriod)")
					return nil
				}
			case .thisMonth:
				let monthComponents = calendar.dateComponents([.month, .year], from: Date())
				if let monthStart = calendar.date(from: monthComponents) {
					return monthStart
				} else {
					return nil
				}
			case .yesterday:
				let todayComponents = calendar.dateComponents([.day, .month, .year], from: Date())
				let today = calendar.date(from: todayComponents)!
				return calendar.date(byAdding: .day, value: -1, to: today)
			case .specificDay(day: let targetDate):
				let startOfDayComps = calendar.dateComponents([.day, .month, .year], from: targetDate)
				return calendar.date(from: startOfDayComps)!
			case .any:
				return Date().addingTimeInterval(-Date.timeIntervalSinceReferenceDate)
			default:
				return nil
        }
    }
    
}
