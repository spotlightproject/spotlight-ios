import UIKit

extension UIAlertAction {
	static var cancelAction: UIAlertAction {
		UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
	}
}
