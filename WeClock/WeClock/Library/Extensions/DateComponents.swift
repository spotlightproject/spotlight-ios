import Foundation

extension DateComponents {
    
	internal static func interval(from timePeriod: HKModel.TimePeriod) -> DateComponents {
        switch timePeriod {
        case .today: return DateComponents(day: 1)
		case .specificDay(day: _): return DateComponents(day: 1)
        case .thisWeek: return DateComponents(day: 7)
        case .thisMonth: return DateComponents(month: 1)
        default: return DateComponents()
        }
    }
}
