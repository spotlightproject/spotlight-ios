import UIKit

extension UIView {
	
	public func snap(to container: UIView) {
		container.addSubview(self)
		self.translatesAutoresizingMaskIntoConstraints = false
		self.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
		self.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
		self.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
		self.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
	}
	
	public func snap(to container: UIView, offset: CGFloat) {
		container.addSubview(self)
		self.translatesAutoresizingMaskIntoConstraints = false
		self.topAnchor.constraint(equalTo: container.topAnchor, constant: offset).isActive = true
		self.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: offset).isActive = true
		self.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: offset).isActive = true
		self.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: offset).isActive = true
	}
	
	public func snap(to container: UIView, inset: CGFloat) {
		container.addSubview(self)
		self.translatesAutoresizingMaskIntoConstraints = false
		self.topAnchor.constraint(equalTo: container.topAnchor, constant: inset).isActive = true
		self.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: inset).isActive = true
		self.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -inset).isActive = true
		self.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -inset).isActive = true
	}
	
	public func pin(beneath view: UIView, offset: CGFloat = 0) {
		self.translatesAutoresizingMaskIntoConstraints = false
		self.topAnchor.constraint(equalTo: view.bottomAnchor, constant: offset).isActive = true
	}
	
	public func pin(above view: UIView, offset: CGFloat = 0) {
		self.translatesAutoresizingMaskIntoConstraints = false
		self.topAnchor.constraint(equalTo: view.topAnchor, constant: offset).isActive = true
	}
}
