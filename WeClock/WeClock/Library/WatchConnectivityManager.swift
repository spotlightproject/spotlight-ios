import Foundation
import WatchConnectivity

class WatchConnectivityManager: NSObject {
	
	//MARK: Shared Instance
	public static var shared = WatchConnectivityManager()
	
	//MARK: Initialization
	override internal init() {
		super.init()
		beginConnectivitySession()
	}
	
	//MARK: Session
	var wcSession: WCSession?
	
	//MARK: Session Configuration
	public func beginConnectivitySession() {
		if WCSession.isSupported() {
			wcSession = WCSession.default
			wcSession?.delegate = self
			
			guard let session = wcSession else {
				Settings.shared.hasPairedAppleWatch.object = false
				return
			}
			
			
			session.activate()
		}
	}
	
	public var wcSessionIsPairedInstalledAndActive: Bool {
		guard let session = wcSession else {
			debug("WCSession is nil")
			return false
		}
		
		if !session.isPaired {
			debug("WCSession failed (pairing error)")
			return false
		}
		
		if !session.isWatchAppInstalled {
			debug("WCSession failed (installation error)")
			return false
		}
		
		if session.activationState == .activated {
			return true
		} else {
			return false
		}
	}
}

extension WatchConnectivityManager: WCSessionDelegate {
	func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
		
		/// Record paired status in settings
		if session.isPaired {
			Settings.shared.hasPairedAppleWatch.object = true
		} else {
			Settings.shared.hasPairedAppleWatch.object = false
		}
	}
	
	func sessionDidBecomeInactive(_ session: WCSession) {
		debug("session inactive")
	}
	
	func sessionDidDeactivate(_ session: WCSession) {
		debug("session active")
	}
	
	func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
		debug("user info receieved!", data: userInfo)
		WatchTransferManager().save(workLogUserInfo: userInfo)
	}
}
