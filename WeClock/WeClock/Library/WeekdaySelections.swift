import Foundation

struct WeekdaySelections {
	var selected: [Weekday] = []
	
	var unselected: [Weekday] {
		var results: [Weekday] = []
		for weekday in Weekday.allCases {
			if !selected.contains(weekday) {
				results.append(weekday)
			}
		}
		return results
	}
	
	mutating func select(weekday: Weekday) {
		selected.append(weekday)
	}
	
	mutating func select(weekdays: [Weekday]) {
		for weekday in weekdays {
			select(weekday: weekday)
		}
	}
	
	//For saving to User Defaults
	var dictionary: [String: Bool] {
		var results: [String: Bool] = [:]
		for weekday in selected {
			results[weekday.rawValue] = true
		}
		for weekday in unselected {
			results[weekday.rawValue] = false
		}
		return results
	}
	
}

enum Weekday: String, CaseIterable {
	case monday = "Monday"
	case tuesday = "Tuesday"
	case wednesday = "Wednesday"
	case thursday = "Thursday"
	case friday = "Friday"
	case saturday = "Saturday"
	case sunday = "Sunday"
}
