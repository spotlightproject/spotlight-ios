import UIKit
import MapKit

class MapComponent: UIView {
	
	//MARK: Properties
	public var mapView = MKMapView()
	private let mapImage = UIImage(systemName: "map")
	public let label = UILabel()
	private lazy var imageView: UIImageView = {
		UIImageView(image: mapImage)
	}()
	
	public var animationDelay: TimeInterval = 1
	public var animationDuration: TimeInterval = 0.25
	
	//MARK: Initialization
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	func setup() {
		let cardColor = (UIColor(named: "foreground") ?? .white)
		self.backgroundColor = cardColor
		self.addShadows(color: UIColor(named: "shadow") ?? .black, opacity: 0.1, radius: 3)
		self.layer.cornerRadius = 20
		self.layer.borderColor = cardColor.cgColor
		self.layer.borderWidth = 3
		
		self.addSubview(mapView)
		mapView.translatesAutoresizingMaskIntoConstraints = false
		mapView.alpha = 0
		mapView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
		mapView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
		mapView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
		mapView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
		mapView.layer.cornerRadius = 20
		mapView.clipsToBounds = true
		
		self.addSubview(imageView)
		imageView.translatesAutoresizingMaskIntoConstraints = false
		imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -15).isActive = true
		imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
		imageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2).isActive = true
		imageView.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2).isActive = true
		
		self.addSubview(label)
		label.translatesAutoresizingMaskIntoConstraints = false
		label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
		label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10).isActive = true
		label.text = "Map loading"
		label.textColor = UIColor(named: "brand-primary")!
		label.font = UIFont.preferredFont(forTextStyle: .headline)
	}
	
	//MARK: Animations
	public func animate() {
		let pulseAnimation = CABasicAnimation(keyPath: "transform.scale")
		pulseAnimation.duration = 1
		pulseAnimation.fromValue = 1
		pulseAnimation.toValue = 1.1
		pulseAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
		pulseAnimation.autoreverses = true
		pulseAnimation.repeatCount = .greatestFiniteMagnitude
		imageView.layer.add(pulseAnimation, forKey: "pulse")
	}
	
	//MARK: Control
	public func showMap() {
		UIView.animate(withDuration: animationDuration, delay: animationDelay, options: .curveEaseInOut, animations: {
			self.mapView.alpha = 1
			self.imageView.alpha = 0
			self.label.alpha = 0
		}) { (success) in
			debug("Done animating")
			self.imageView.layer.removeAnimation(forKey: "pulse")
		}
	}
	
	public func showLoading() {
		UIView.animate(withDuration: animationDuration) {
			self.mapView.alpha = 0
			self.imageView.alpha = 1
			self.label.alpha = 1
		}
		animate()
	}
}
