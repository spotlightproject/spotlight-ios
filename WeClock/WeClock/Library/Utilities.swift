import Foundation

fileprivate var ConsolePrefix = ">>"

func dprint(_ input: Any?, data: Any? = nil) {
	#if DEBUG
		var output: String
		var additionalOutput: String? = nil
	
		if let data = data {
			additionalOutput = String(describing: data)
		}
		
		if let unwrapped = input {
			let string = String(describing: unwrapped)
			output = "\(string)"
		} else {
			output = "nil"
		}
	
		//Print debug event
		print("\(ConsolePrefix) \(output)")
		if let additionalOutput = additionalOutput {
			print("\(ConsolePrefix) { \(additionalOutput) }")
		}
	#endif
}

func debug(_ input: Any?, save: Bool = false, data: Any? = nil) {
		var output: String
		var additionalOutput: String? = nil
	
		if let data = data {
			additionalOutput = String(describing: data)
		}
		
		if let unwrapped = input {
			let string = String(describing: unwrapped)
			output = "\(string)"
		} else {
			output = "nil"
		}
	
		//Print debug event
		print("\(ConsolePrefix) \(output)")
		if let additionalOutput = additionalOutput {
			print("\(ConsolePrefix) { \(additionalOutput) }")
		}
		
		//Optionally save debug event
	
		if save {
			let backgroundContext = CoreData.shared.backgroundContext
			backgroundContext.perform {
				let debugEvent = DebugEvent(context: backgroundContext)
				debugEvent.eventDescription = output
				debugEvent.objectDescription = additionalOutput ?? "No object"
				debugEvent.dateRecorded = Date()
				do {
					try backgroundContext.save()
				} catch {
					debug("Core Data save error", data: error)
				}
			}
		}
}


