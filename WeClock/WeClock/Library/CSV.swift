import Foundation

class CSV {
	
	//MARK: Definitions
	public typealias Row = [String]
	
	//MARK: Properties
	public var separator = ","
	private var rowIndex = 0
	
	//MARK: Public Methods
	public var string: String = String()
	
	public func add(rows: [Row]) {
		for row in rows {
			add(row: row)
		}
	}
	
	public func add(row: [String]) {
		addIndex()
		addSeparator()
		for (index, string) in row.enumerated() {
			add(value: string)
			if index < row.lastIndex {
				addSeparator()
			}
		}
		addLineBreak()
	}
	
	//MARK: Row Constructors
	private func addIndex() {
		string += "\(rowIndex)"
		rowIndex += 1
	}
	
	private func addLineBreak() {
		string += "\n"
	}
	
	private func add(value: String) {
		string += value
	}
	
	private func addSeparator() {
		string += separator
	}
}

//MARK: File Writing
extension CSV {
	public func save() {
		let path = temporaryURL.path
		do {
			try string.write(toFile: path, atomically: true, encoding: .utf8)
			debug("done writing")
		} catch {
			debug("Error writing string", data: error)
		}
	}
	
	public var temporaryURL: URL {
		getDocumentsDirectory().appendingPathComponent("data.csv")
	}
	
	func getDocumentsDirectory() -> URL {
		let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		let documentsDirectory = paths.first!
		return documentsDirectory
	}
}

extension Array {
	var lastIndex: Int {
		self.count - 1
	}
}
