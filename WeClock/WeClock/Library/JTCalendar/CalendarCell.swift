import UIKit
import JTAppleCalendar

class CalendarCell: JTACDayCell {
    
	let dateLabel: UILabel
	let selectedView = UIView()
	var selectedHeightConstraint: NSLayoutConstraint!
	
	@IBInspectable
	var cornerRadius: CGFloat {
		get { layer.cornerRadius }
		set { layer.cornerRadius = newValue }
	}
	
	//MARK: Initialization
	required init?(coder: NSCoder) {
		dateLabel = UILabel()
		super.init(coder: coder)
		setup()
	}
	
	override init(frame: CGRect) {
		dateLabel = UILabel()
		super.init(frame: frame)
		setup()
	}
	
	private func setup() {
		self.contentView.addSubview(selectedView)
		selectedView.translatesAutoresizingMaskIntoConstraints = false
		selectedView.heightAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
		selectedView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
		selectedView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
		selectedView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
		selectedView.backgroundColor = UIColor(named: "calendar-cell-selected")
		
		
		self.contentView.addSubview(dateLabel)
		dateLabel.translatesAutoresizingMaskIntoConstraints = false
		dateLabel.adjustsFontSizeToFitWidth = true
		dateLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
		dateLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		selectedView.layer.cornerRadius = self.frame.size.width / 2
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		dateLabel.text = ""
	}
}
