import JTAppleCalendar
import Foundation

class CalDataSource: JTACMonthViewDataSource {
	func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy MM dd"
		let startDate = formatter.date(from: "2020 01 01")!
		let endDate = Date()
		let config = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 6, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfGrid, firstDayOfWeek: .sunday, hasStrictBoundaries: false)
		return config
	}
}
