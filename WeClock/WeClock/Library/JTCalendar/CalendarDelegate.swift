import Foundation
import JTAppleCalendar
import Combine
import UIKit

class CalDelegate: JTACMonthViewDelegate {
	
	func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
	   let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! CalendarCell
	   self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
	   return cell
	}
		
	func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
	   configureCell(view: cell, cellState: cellState)
	}
	
	func configureCell(view: JTACDayCell?, cellState: CellState) {
		guard let cell = view as? CalendarCell  else { return }
		cell.dateLabel.text = cellState.text
		handleCellTextColor(cell: cell, cellState: cellState)
		handleCellSelected(cell: cell, cellState: cellState)
	}
	
	func handleCellSelected(cell: CalendarCell, cellState: CellState) {
		if cellState.isSelected {
			cell.selectedView.isHidden = false
			cell.dateLabel.textColor = UIColor(named: "calendar-text-selected")
		} else {
			cell.selectedView.isHidden = true
			cell.dateLabel.textColor = UIColor(named: "calendar-text-unselected")
		}
	}
		
	func handleCellTextColor(cell: CalendarCell, cellState: CellState) {
	   if cellState.dateBelongsTo == .thisMonth {
		  cell.isHidden = false
	   } else {
		  cell.isHidden = true
	   }
	}
	
	func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
		configureCell(view: cell, cellState: cellState)
		proxyViewModel?.filter(by: .specificDay(day: date))
	}
	
	func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
		configureCell(view: cell, cellState: cellState)
	}
	
	public var proxyViewModel: Filterable?
}
