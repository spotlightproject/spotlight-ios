import UIKit

class DebugEventVC: UIViewController {
	
	//MARK: Properties
	public var debugEvent: DebugEvent!
	
	//MARK: IBOutlets
	@IBOutlet weak var detailsEventTextView: UITextView!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		detailsEventTextView.text = debugEvent.objectDescription
	}
}
