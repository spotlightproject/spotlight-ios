import UIKit
import CoreData

class DebugEventTableVC: UITableViewController, NSFetchedResultsControllerDelegate {
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	//MARK: UITableView Data Source
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return frc.fetchedObjects?.count ?? 0
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
		let event = frc.fetchedObjects?[indexPath.row]
		cell.textLabel?.text = event?.eventDescription
		cell.detailTextLabel?.text = event?.dateRecorded?.description(with: Calendar.current.locale)
		return cell
	}
	
	private lazy var frc: NSFetchedResultsController<DebugEvent> = {
		let fetch = DebugEvent.fetchRequest() as NSFetchRequest<DebugEvent>
		fetch.predicate = nil
		fetch.sortDescriptors = [NSSortDescriptor(keyPath: \DebugEvent.dateRecorded, ascending: false)]
		fetch.fetchLimit = 1000
		let controller = NSFetchedResultsController(fetchRequest: fetch, managedObjectContext: CoreData.shared.viewContext, sectionNameKeyPath: nil, cacheName: nil)
		
		controller.delegate = self
		try! controller.performFetch()
		return controller
	}()
	
	//MARK: UITableView Delegate
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let selectedEvent = frc.fetchedObjects?[indexPath.row] else {
			debug("Couldn't find object in fetchedObjects at index \(indexPath.row)"); return
		}
		
		let storyboard = UIStoryboard(name: "Debugging", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "debugEvent") as DebugEventVC
		vc.debugEvent = selectedEvent
		
		navigationController?.pushViewController(vc, animated: true)
	}
	
	//MARK: NSFetchedResultsController Delegate
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.beginUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		switch type {
			case .insert: tableView.insertRows(at: [newIndexPath!], with: .automatic)
			default: debug("Default")
		}
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.beginUpdates()
	}
	
}
