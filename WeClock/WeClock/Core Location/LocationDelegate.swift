import CoreLocation
import Foundation

class LocationDelegate: NSObject, CLLocationManagerDelegate {
	
	//MARK: Receiving Events
	func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
		let clLocation = CLLocation(latitude: visit.coordinate.latitude, longitude: visit.coordinate.longitude)
		debug("didVisit", save: true, data: visit)
	
		//Save location of visit to core data
		StoredLocation.save(clLocation, in: CoreData.shared.backgroundContext, service: "Visits", accuracy: visit.horizontalAccuracy)
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		debug("didUpdateLocations (\(locations.count))", save: true, data: locations)
		
		//Save most recent location
		if let mostRecentLocation = locations.last {
			StoredLocation.save(mostRecentLocation, in: CoreData.shared.backgroundContext, service: "Standard", accuracy: mostRecentLocation.horizontalAccuracy)
		}
	}
	
	//MARK: Event Flow Status
	func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
		debug("locationManager didEnterRegion", save: true, data: region)
	}
	
	func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
		debug("locationManager didExitRegion", save: true, data: region)
	}
		
	func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
		debug("locationManager monitoringDidFail region", save: true, data: error)
	}
	
	func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
		debug("locationManager didFinishedUpdatesWithError", save: true, data: error)
	}
	
	func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
		debug("locationManager didResumeUpdates", save: true)
	}
	
	func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
		debug("locationManager updatesPaused", save: true)
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		debug("locationManager didFail", save: true, data: error)
	}
	
	//MARK: Authorization Status
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		var description: String
		switch status.rawValue {
			case 0: description = "0 - Not Determined"
			case 1: description = "1 - Restricted"
			case 2: description = "2 - Denied"
			case 3: description = "3 - Always Authorized"
			case 4: description = "4 - Authorized When in Use"
			default: description = "Unable to read status object"
		}
		debug("locationManager authStatusChanged", save: true, data: description)
	}

}
