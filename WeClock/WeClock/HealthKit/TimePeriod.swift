import Foundation
import HealthKit

public extension HKModel {
	
	enum TimePeriod {
		case today
		case yesterday
		case thisWeek
		case thisMonth
		case thisYear
		case specificDay(day: Date)
		case any
	}

}
