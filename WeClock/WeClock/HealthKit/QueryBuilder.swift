import HealthKit
import Combine

extension HKModel {
	
	enum QueryBuilder {
		
		/*
			Returns a pre-configured HKStatisticsCollectionQuery. The completion handler will be called both when the query returns initial results and when it returns an update.
		
			We don't actually need to return a collection of multiple HKStatistics objects. We just need the updateHandler functionality provided by this query to receive updates. Thus the interval is the same length as the predicate. (The collection will be one result encompassing the entire time period).
		
			-Use this builder for statistical queries that need to be updated when the data changes.
		*/
		public static func collectionQuery(identifier: HKQuantityTypeIdentifier, timePeriod: TimePeriod, options: HKStatisticsOptions, completion: @escaping (HKStatistics) -> Void) -> HKStatisticsCollectionQuery? {
			guard let type = HKQuantityType.quantityType(forIdentifier: identifier) else {
			   debug("Couldn't construct type from identifier: \(identifier)")
			   return nil
			}

			guard let anchor = Date.startDate(of: timePeriod) else {
			   debug("Couldn't construct date from timePeriod: \(timePeriod)")
			   return nil
			}

			let interval = DateComponents.interval(from: timePeriod)
			let predicate = HKModel.Predicate.from(timePeriod)
			

			let collectionQuery = HKStatisticsCollectionQuery(quantityType: type, quantitySamplePredicate: predicate, options: options, anchorDate: anchor, intervalComponents: interval)


			collectionQuery.initialResultsHandler = { query, collection, error in
				if let error = error {
					debug("Query error: \(error)")
				}
				
				if collection == nil {
					debug("no collection produced")
				}
				
				if let collection = collection {
					if let stats = collection.statistics(for: anchor) {
						completion(stats)
					} else {
						debug("no stats")
					}
				}
			}
		
			
			collectionQuery.statisticsUpdateHandler = { query, _, collection, error in
				if let error = error {
					debug("Query error: \(error)")
				}
				
				if collection == nil {
					debug("no collection produced")
				}
				
				if let collection = collection {
					if let stats = collection.statistics(for: anchor) {
						completion(stats)
					} else {
						debug("no stats")
					}
				}
			}
			return collectionQuery
		}
	
		/*
			Returns a pre-configured HKAnchoredObjectQuery. Provide two completion handlers, one for the inital result and updates; one for deleted objects.
		
			-Use this builder for sample queries that receive updates upon deletion or insert. (If you're populating a table view you will probably use this.)
		*/
		public static func anchoredQuery(identifier: HKQuantityTypeIdentifier, timePeriod: TimePeriod, completion: @escaping ([HKQuantitySample]) -> Void, deletionHandler: @escaping ([HKDeletedObject]) -> Void) -> HKAnchoredObjectQuery? {
			
			guard let type = HKQuantityType.quantityType(forIdentifier: identifier) else {
				debug("Couldn't construct type from identifier", data: identifier)
				return nil
			}

			let predicate = HKModel.Predicate.from(timePeriod)
			
			let query = HKAnchoredObjectQuery(type: type, predicate: predicate, anchor: nil, limit: HKObjectQueryNoLimit) { (_, samplesOrNil, _, _, error) in
				if let error = error { debug("Query error", data: error) }
				if let samples = samplesOrNil as? [HKQuantitySample] {
					completion(samples)
				}
			}
			
			query.updateHandler = { (query, samplesOrNil, deletionsOrNil, anchor, error) in
				if let deletedObjects = deletionsOrNil {
					if !deletedObjects.isEmpty { deletionHandler(deletedObjects) }
				}
			}
			
			return query
		}
	
		public static func query(identifier: HKQuantityTypeIdentifier, timePeriod: TimePeriod, completion: @escaping ([HKQuantitySample]) -> Void) -> HKSampleQuery? {
			guard let type = HKQuantityType.quantityType(forIdentifier: identifier) else {
				debug("Couldn't construct type from identifier", data: identifier)
				return nil
			}

			let predicate = HKModel.Predicate.from(timePeriod)
			let sortDescriptor = NSSortDescriptor(keyPath: \HKSample.startDate, ascending: false)
			let query = HKSampleQuery(sampleType: type, predicate: predicate, limit: 1000, sortDescriptors: [sortDescriptor]) { (query, samplesOrNil, errorOrNil) in
				if let error = errorOrNil {
					debug("Query error", data: error)
				}
				
				if let samples = samplesOrNil as? [HKQuantitySample] {
					completion(samples)
				}
			}
			return query
		}
		
		/// Produces a query wrapped in a publisher that finishes when samples are returned.
		///
		/// The publisher cannot fail. If the query fails, the publisher simply produces an empty array. This is useful for the `zip`
		/// operator, which will hang if a zipped publisher doesn't produce an element.
		/// - Parameters:
		///   - identifier: The type identifier for the desired samples.
		///   - timePeriod: The time period to fetch from.
		public static func future(identifier: HKQuantityTypeIdentifier, timePeriod: TimePeriod) -> Future<[HKQuantitySample], Never>? {
			guard let type = HKQuantityType.quantityType(forIdentifier: identifier) else {
				debug("Couldn't construct type from identifier", data: identifier)
				return nil
			}

			let predicate = HKModel.Predicate.from(timePeriod)
			let sortDescriptor = NSSortDescriptor(keyPath: \HKSample.startDate, ascending: false)
			
			let future = Future<[HKQuantitySample], Never> { (fullfillment) in
				let query = HKSampleQuery(sampleType: type, predicate: predicate, limit: 500, sortDescriptors: [sortDescriptor]) { (_, samplesOrNil, errorOrNil) in
					if let error = errorOrNil  {
						debug("Query error", data: error)
						fullfillment(.success([]))
					}
			
					let samples = samplesOrNil as? [HKQuantitySample]
					fullfillment(.success(samples ?? []))
				}
				
				HKModel.shared.execute(query)
			}
			
			return future
		}
	}
}
