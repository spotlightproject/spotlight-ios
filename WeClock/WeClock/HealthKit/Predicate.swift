import HealthKit

extension HKModel {
	
	enum Predicate {
		
		static func from(_ timePeriod: TimePeriod) -> NSPredicate? {
			let calendar = Calendar.current
			let todayComponents = calendar.dateComponents([.day, .month, .year], from: Date())
			let today = calendar.date(from: todayComponents)!
			
			switch timePeriod {
				case .today:
					let tomorrow = calendar.date(byAdding: .day, value: 1, to: today)
					let predicate = HKQuery.predicateForSamples(withStart: today, end: tomorrow, options: [])
					return predicate
				case .yesterday:
					let yesterday = calendar.date(byAdding: .day, value: -1, to: today)
					let predicate = HKQuery.predicateForSamples(withStart: yesterday, end: today, options: [])
					return predicate
				case .thisWeek:
					let weekComponents = calendar.dateComponents([.weekOfYear, .yearForWeekOfYear], from: Date())
					let calendarWeekStart = calendar.date(from: weekComponents)!
					let weekByMondayEnd = calendar.date(byAdding: .day, value: 7, to: calendarWeekStart)
					let predicate = HKQuery.predicateForSamples(withStart: calendarWeekStart, end: weekByMondayEnd, options: [])
					return predicate
				case .thisMonth:
					let monthComponents = calendar.dateComponents([.month, .year], from: Date())
					let monthStart = calendar.date(from: monthComponents)!
					let monthEnd = calendar.date(byAdding: .month, value: 1, to: monthStart)
					let predicate = HKQuery.predicateForSamples(withStart: monthStart, end: monthEnd, options: [])
					return predicate
				case .thisYear:
					let yearComponent = calendar.dateComponents([.year], from: Date())
					let yearStart = calendar.date(from: yearComponent)!
					let yearEnd = calendar.date(byAdding: .year, value: 1, to: yearStart)
					let predicate = HKQuery.predicateForSamples(withStart: yearStart, end: yearEnd, options: [])
					return predicate
				case .specificDay(day: let targetDate):
					let startOfDayComps = calendar.dateComponents([.day, .month, .year], from: targetDate)
					let startOfDay = calendar.date(from: startOfDayComps)!
					let endOfDay = calendar.date(byAdding: DateComponents(day: 1), to: startOfDay)!
					let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: endOfDay, options: [])
					return predicate
				case .any:
					return nil
			}
		}
	}
}
