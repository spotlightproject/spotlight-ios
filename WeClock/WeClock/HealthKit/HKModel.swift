import HealthKit

/*
	An interface for HKHealthStore.
*/
public class HKModel {
	
	//MARK: Shared Instance
    public static var shared: HKModel = HKModel()
	
	//MARK: Properties
    private var activeQueries = [HKQuery]()
    public let store = HKHealthStore()
    
    //MARK: Query Management
    public func stopQueries() {
        activeQueries.forEach { store.stop($0) }
        activeQueries.removeAll(keepingCapacity: true)
    }
	
	//MARK: Executing Queries
	public func execute(_ query: HKQuery?) {
		if let query = query {
			store.execute(query)
		} else {
			debug("Failure to execute HKQuery. It was nil. Check that it's being built correctly.")
		}
	}
	
	public func execute(_ queries: [HKQuery?]) {
		queries.forEach { execute($0) }
	}
	
	//MARK: - Debugging
	func addSteps(_ amount: Double) {
		let stepType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
		let yesterday = Date.startDate(of: .yesterday)!
		
		let quantity = HKQuantity(unit: .count(), doubleValue: amount)
		let sample = HKQuantitySample(type: stepType, quantity: quantity, start: yesterday, end: yesterday)
		HKModel.shared.store.save(sample) { (success, error) in
			if let error = error { debug("Error saving: \(error)")}
			if success {
				debug("Successfully saved sample!")
			}
		}
	}
}
