import HealthKit

extension HKQuantityType {
	
	public static func quantityTypes(for identifiers: [HKQuantityTypeIdentifier]) -> Set<HKSampleType> {
		var results: Set<HKSampleType> = []
		
		for identifier in identifiers {
			if let type = HKQuantityType.quantityType(forIdentifier: identifier) {
				results.insert(type)
			}
		}
		
		return results
	}
}
